import app.NotificationServiceApp;

/**
 * @author joseph
 */
public class Main {
    public static void main(String[] args) {
        new NotificationServiceApp().start();
    }
}
