package app;

import app.notification.domain.Notification;
import app.notification.handler.CancelReservationMessageHandler;
import app.notification.handler.CreateReservationMessageHandler;
import app.notification.job.CompleteReservationJob;
import app.notification.job.SendEmailJob;
import app.notification.service.NotificationService;
import app.reservation.api.ReservationWebService;
import app.reservation.api.kafka.CancelReservationMessage;
import app.reservation.api.kafka.CreateReservationMessage;
import core.framework.module.KafkaConfig;
import core.framework.module.Module;
import core.framework.module.SchedulerConfig;

import java.time.Duration;

/**
 * @author joseph
 */
public class NotificationModule extends Module {
    @Override
    protected void initialize() {
        db().repository(Notification.class);
        bind(NotificationService.class);
        api().client(ReservationWebService.class, requiredProperty("app.reservationWebService.url"));

        KafkaConfig kafkaConfig = kafka();
        kafkaConfig.groupId("group1");
        kafkaConfig.subscribe("create-reservation", CreateReservationMessage.class, bind(CreateReservationMessageHandler.class));
        kafkaConfig.subscribe("cancel-reservation", CancelReservationMessage.class, bind(CancelReservationMessageHandler.class));

        SchedulerConfig schedulerConfig = schedule();
        schedulerConfig.fixedRate("send-email-job", bind(SendEmailJob.class), Duration.ofMinutes(1));
        schedulerConfig.fixedRate("complete-reservation-job", bind(CompleteReservationJob.class), Duration.ofMinutes(1));
    }
}
