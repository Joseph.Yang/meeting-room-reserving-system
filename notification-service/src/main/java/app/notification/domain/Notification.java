package app.notification.domain;

import core.framework.api.validate.NotNull;
import core.framework.db.Column;
import core.framework.db.PrimaryKey;
import core.framework.db.Table;

import java.time.ZonedDateTime;

@Table(name = "notifications")
public class Notification {
    @PrimaryKey(autoIncrement = true)
    @Column(name = "id")
    public Integer id;

    @NotNull
    @Column(name = "employee_email")
    public String employeeEmail;

    @NotNull
    @Column(name = "notify_time")
    public ZonedDateTime notifyTime;

    @NotNull
    @Column(name = "complete_time")
    public ZonedDateTime completeTime;

    @NotNull
    @Column(name = "reservation_id")
    public String reservationId;

    @NotNull
    @Column(name = "status")
    public NotificationStatus status;
}
