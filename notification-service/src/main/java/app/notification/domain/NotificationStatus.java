package app.notification.domain;

import core.framework.db.DBEnumValue;

/**
 * @author joseph
 */
public enum NotificationStatus {
    @DBEnumValue("READY")
    READY,
    @DBEnumValue("SENT")
    SENT,
    @DBEnumValue("CANCELED")
    CANCELED,
    @DBEnumValue("COMPLETED")
    COMPLETED
}
