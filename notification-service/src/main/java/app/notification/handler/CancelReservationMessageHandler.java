package app.notification.handler;

import app.notification.service.NotificationService;
import app.reservation.api.kafka.CancelReservationMessage;
import core.framework.inject.Inject;
import core.framework.kafka.MessageHandler;
import core.framework.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author joseph
 */
public class CancelReservationMessageHandler implements MessageHandler<CancelReservationMessage> {
    private final Logger logger = LoggerFactory.getLogger(CancelReservationMessageHandler.class);

    @Inject
    private NotificationService notificationService;

    @Override
    public void handle(String key, CancelReservationMessage value) throws Exception {
        notificationService.cancel(value.reservationId);
        logger.warn(Strings.format("reservation is cancel, id = {}"), value.reservationId);
    }
}
