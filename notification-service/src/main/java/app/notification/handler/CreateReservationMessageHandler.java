package app.notification.handler;

import app.notification.domain.Notification;
import app.notification.domain.NotificationStatus;
import app.notification.service.NotificationService;
import app.reservation.api.kafka.CreateReservationMessage;
import core.framework.inject.Inject;
import core.framework.kafka.MessageHandler;
import core.framework.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author joseph
 */
public class CreateReservationMessageHandler implements MessageHandler<CreateReservationMessage> {
    private final Logger logger = LoggerFactory.getLogger(CreateReservationMessageHandler.class);

    @Inject
    private NotificationService notificationService;


    @Override
    public void handle(String key, CreateReservationMessage value) throws Exception {
        Notification notification = new Notification();
        notification.employeeEmail = value.employeeEmail;
        notification.notifyTime = value.notifyTime;
        notification.completeTime = value.completeTime;
        notification.reservationId = value.reservationId;
        notification.status = NotificationStatus.READY;
        notificationService.create(notification);
        logger.warn(Strings.format("create a notification, employee email = {}, notify time = {}", value.employeeEmail, value.notifyTime));
    }
}
