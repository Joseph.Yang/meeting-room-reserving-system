package app.notification.job;

import app.notification.domain.Notification;
import app.notification.domain.NotificationStatus;
import app.reservation.api.ReservationWebService;
import core.framework.db.Query;
import core.framework.db.Repository;
import core.framework.inject.Inject;
import core.framework.scheduler.Job;
import core.framework.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.ZonedDateTime;

/**
 * @author joseph
 */
public class CompleteReservationJob implements Job {
    private final Logger logger = LoggerFactory.getLogger(CompleteReservationJob.class);

    @Inject
    private Repository<Notification> notificationRepository;

    @Inject
    private ReservationWebService reservationWebService;

    @Override
    public void execute() throws Exception {
        Query<Notification> query = notificationRepository.select();
        query.where("status in (?,?)", NotificationStatus.SENT, NotificationStatus.READY);
        query.where("complete_time <= ?", ZonedDateTime.now());
        if (!query.fetch().isEmpty()) {
            query.fetch().forEach(notification -> {
                reservationWebService.complete(notification.reservationId);
                notification.status = NotificationStatus.COMPLETED;
                notificationRepository.partialUpdate(notification);
                logger.warn(Strings.format("reservation is end, id = {}", notification.reservationId));
            });
        }
    }
}
