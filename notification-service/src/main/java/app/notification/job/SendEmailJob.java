package app.notification.job;

import app.notification.domain.Notification;
import app.notification.domain.NotificationStatus;
import core.framework.db.Query;
import core.framework.db.Repository;
import core.framework.inject.Inject;
import core.framework.scheduler.Job;
import core.framework.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.ZonedDateTime;

/**
 * @author joseph
 */
public class SendEmailJob implements Job {
    private final Logger logger = LoggerFactory.getLogger(SendEmailJob.class);

    @Inject
    private Repository<Notification> notificationRepository;

    @Override
    public void execute() throws Exception {
        Query<Notification> query = notificationRepository.select();
        query.where("status = ?", NotificationStatus.READY);
        query.where("notify_time <= ?", ZonedDateTime.now());
        if (!query.fetch().isEmpty()) {
            query.fetch().forEach(notification -> {
                if (notification.completeTime.compareTo(ZonedDateTime.now()) >= 0) {
                    logger.warn(Strings.format("send email to {}", notification.employeeEmail));
                    notification.status = NotificationStatus.SENT;
                    notificationRepository.partialUpdate(notification);
                }
            });
        }
    }
}
