package app.notification.service;

import app.notification.domain.Notification;
import app.notification.domain.NotificationStatus;
import core.framework.db.Repository;
import core.framework.inject.Inject;
import core.framework.web.exception.NotFoundException;

public class NotificationService {
    @Inject
    private Repository<Notification> notificationRepository;

    public void create(Notification notification) {
        notificationRepository.insert(notification);
    }

    public void cancel(String reservationId) {
        Notification notification = notificationRepository.selectOne("reservation_id = ?", reservationId).orElseThrow(() ->
            new NotFoundException("notification not found, id= ", reservationId));
        notification.status = NotificationStatus.CANCELED;
        notificationRepository.partialUpdate(notification);
    }
}
