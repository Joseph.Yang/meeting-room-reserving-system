package app;

import app.company.api.BORoomWebService;
import app.company.api.RoomWebService;
import app.reservation.api.BOReservationWebService;
import app.room.service.BORoomService;
import app.room.service.RoomService;
import app.room.web.BORoomWebServiceImpl;
import app.room.web.RoomWebServiceImpl;
import core.framework.module.Module;

/**
 * @author joseph
 */
public class RoomModule extends Module {
    @Override
    protected void initialize() {
        api().client(BOReservationWebService.class, requiredProperty("app.reservationWebService.url"));
        bind(RoomService.class);
        bind(BORoomService.class);
        api().service(RoomWebService.class, bind(RoomWebServiceImpl.class));
        api().service(BORoomWebService.class, bind(BORoomWebServiceImpl.class));
    }
}
