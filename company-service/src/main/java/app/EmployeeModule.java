package app;

import app.company.api.BOEmployeeWebService;
import app.company.api.EmployeeWebService;
import app.employee.service.BOEmployeeService;
import app.employee.service.EmployeeService;
import app.employee.web.BOEmployeeWebServiceImpl;
import app.employee.web.EmployeeWebServiceImpl;
import core.framework.module.Module;

/**
 * @author joseph
 */
public class EmployeeModule extends Module {
    @Override
    protected void initialize() {
        bind(EmployeeService.class);
        bind(BOEmployeeService.class);
        api().service(EmployeeWebService.class, bind(EmployeeWebServiceImpl.class));
        api().service(BOEmployeeWebService.class, bind(BOEmployeeWebServiceImpl.class));
    }
}
