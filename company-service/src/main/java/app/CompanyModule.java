package app;

import app.company.api.BOCompanyWebService;
import app.company.api.CompanyWebService;
import app.company.domain.Company;
import app.company.service.BOCompanyService;
import app.company.service.CompanyService;
import app.company.web.BOCompanyWebServiceImpl;
import app.company.web.CompanyWebServiceImpl;
import app.employee.domain.Employee;
import app.reservation.api.ReservationWebService;
import app.room.domain.Room;
import core.framework.module.Module;

/**
 * @author joseph
 */
public class CompanyModule extends Module {
    @Override
    protected void initialize() {
        db().repository(Company.class);
        db().repository(Employee.class);
        db().repository(Room.class);
        api().client(ReservationWebService.class, requiredProperty("app.reservationWebService.url"));
        bind(CompanyService.class);
        bind(BOCompanyService.class);
        api().service(CompanyWebService.class, bind(CompanyWebServiceImpl.class));
        api().service(BOCompanyWebService.class, bind(BOCompanyWebServiceImpl.class));
    }
}
