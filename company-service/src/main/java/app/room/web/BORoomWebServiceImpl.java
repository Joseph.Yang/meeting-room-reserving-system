package app.room.web;

import app.company.api.BORoomWebService;
import app.company.api.room.BOCreateRoomRequest;
import app.company.api.room.BOCreateRoomResponse;
import app.company.api.room.BODeleteRoomRequest;
import app.company.api.room.BOSearchRoomRequest;
import app.company.api.room.BOSearchRoomResponse;
import app.company.api.room.BOUpdateRoomRequest;
import app.room.service.BORoomService;
import core.framework.inject.Inject;

/**
 * @author joseph
 */
public class BORoomWebServiceImpl implements BORoomWebService {
    @Inject
    private BORoomService boRoomService;


    @Override
    public BOCreateRoomResponse create(BOCreateRoomRequest request) {
        return boRoomService.create(request);
    }

    @Override
    public BOSearchRoomResponse search(BOSearchRoomRequest request) {
        return boRoomService.search(request);
    }

    @Override
    public void delete(Integer id, BODeleteRoomRequest request) {
        boRoomService.delete(id, request);
    }

    @Override
    public void update(Integer id, BOUpdateRoomRequest request) {
        boRoomService.update(id, request);
    }


}
