package app.room.web;

import app.company.api.RoomWebService;
import app.company.api.room.RoomView;
import app.company.api.room.SearchRoomResponse;
import app.room.service.RoomService;
import core.framework.inject.Inject;

/**
 * @author joseph
 */
public class RoomWebServiceImpl implements RoomWebService {
    @Inject
    private RoomService roomService;

    @Override
    public RoomView get(Integer companyId, Integer roomId) {
        return roomService.get(companyId, roomId);
    }

    @Override
    public SearchRoomResponse search(Integer companyId) {
        return roomService.search(companyId);
    }


}
