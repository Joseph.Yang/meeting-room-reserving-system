package app.room.domain;

import core.framework.db.DBEnumValue;

/**
 * @author joseph
 */
public enum RoomStatus {
    @DBEnumValue("OPEN")
    OPEN,
    @DBEnumValue("CLOSED")
    CLOSED,
    @DBEnumValue("UNABLE")
    UNABLE
}
