package app.room.domain;

import core.framework.api.validate.NotBlank;
import core.framework.api.validate.NotNull;
import core.framework.db.Column;
import core.framework.db.PrimaryKey;
import core.framework.db.Table;

import java.time.ZonedDateTime;

/**
 * @author joseph
 */
@Table(name = "rooms")
public class Room {
    @PrimaryKey(autoIncrement = true)
    @Column(name = "id")
    public Integer id;

    @NotNull
    @NotBlank
    @Column(name = "name")
    public String name;

    @NotNull
    @Column(name = "company_id")
    public Integer companyId;

    @NotNull
    @Column(name = "update_time")
    public ZonedDateTime updateTime;

    @NotNull
    @Column(name = "status")
    public RoomStatus status;

    @NotNull
    @Column(name = "admin_id")
    public String adminId;
}
