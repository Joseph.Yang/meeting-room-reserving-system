package app.room.job;

import app.reservation.api.BOReservationWebService;
import app.reservation.api.reservation.BOSearchReservationRequest;
import app.reservation.api.reservation.ReservationViewStatus;
import app.room.domain.Room;
import app.room.domain.RoomStatus;
import core.framework.db.Repository;
import core.framework.inject.Inject;
import core.framework.scheduler.Job;

import java.time.ZonedDateTime;
import java.util.List;

/**
 * @author joseph
 */
public class AutoCloseRoomJob implements Job {
    @Inject
    private Repository<Room> roomRepository;

    @Inject
    private BOReservationWebService boReservationWebService;

    @Override
    public void execute() throws Exception {
        List<Room> unableRooms = roomRepository.select("status = ?", RoomStatus.UNABLE);
        if (!unableRooms.isEmpty()) {
            unableRooms.forEach(room -> {
                BOSearchReservationRequest boSearchReservationRequest = new BOSearchReservationRequest();
                boSearchReservationRequest.companyId = room.companyId;
                boSearchReservationRequest.roomId = room.id;
                boSearchReservationRequest.status = ReservationViewStatus.RESERVED;
                boSearchReservationRequest.adminId = room.adminId;
                if (boReservationWebService.search(boSearchReservationRequest).reservations.isEmpty()) {
                    room.status = RoomStatus.CLOSED;
                    room.updateTime = ZonedDateTime.now();
                    roomRepository.partialUpdate(room);
                }
            });
        }
    }
}
