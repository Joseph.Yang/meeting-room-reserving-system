package app.room.service;

import app.company.api.room.RoomView;
import app.company.api.room.RoomViewStatus;
import app.company.api.room.SearchRoomResponse;
import app.company.domain.Company;
import app.room.domain.Room;
import app.room.domain.RoomStatus;
import core.framework.db.Query;
import core.framework.db.Repository;
import core.framework.inject.Inject;
import core.framework.web.exception.ForbiddenException;
import core.framework.web.exception.NotFoundException;

import java.util.stream.Collectors;

/**
 * @author joseph
 */
public class RoomService {
    @Inject
    private Repository<Room> roomRepository;

    @Inject
    private Repository<Company> companyRepository;

    public RoomView get(Integer companyId, Integer roomId) {
        Room room = roomRepository.get(roomId).orElseThrow(() -> new NotFoundException("Room not found, id= " + roomId));
        if (!room.companyId.equals(companyId))
            throw new ForbiddenException("You have no access");
        return roomView(room);
    }

    public SearchRoomResponse search(Integer companyId) {
        companyRepository.get(companyId).orElseThrow(() -> new NotFoundException("Company not found, id= " + companyId));
        Query<Room> query = roomRepository.select();
        query.where("company_id = ?", companyId);
        query.where("status = ?", RoomStatus.OPEN);
        SearchRoomResponse searchRoomResponse = new SearchRoomResponse();
        searchRoomResponse.total = query.count();
        searchRoomResponse.rooms = query.fetch().stream().map(this::searchRoomView).collect(Collectors.toList());
        return searchRoomResponse;
    }

    private RoomView roomView(Room room) {
        RoomView roomView = new RoomView();
        roomView.companyId = room.companyId;
        roomView.roomId = room.id;
        roomView.roomName = room.name;
        roomView.status = RoomViewStatus.valueOf(room.status.name());
        return roomView;
    }

    private SearchRoomResponse.SearchRoomView searchRoomView(Room room) {
        SearchRoomResponse.SearchRoomView searchRoomView = new SearchRoomResponse.SearchRoomView();
        searchRoomView.companyId = room.companyId;
        searchRoomView.roomId = room.id;
        searchRoomView.roomName = room.name;
        searchRoomView.status = RoomViewStatus.valueOf(room.status.name());
        return searchRoomView;
    }
}
