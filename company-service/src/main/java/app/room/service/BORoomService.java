package app.room.service;

import app.company.api.room.BOCreateRoomRequest;
import app.company.api.room.BOCreateRoomResponse;
import app.company.api.room.BODeleteRoomRequest;
import app.company.api.room.BOSearchRoomRequest;
import app.company.api.room.BOSearchRoomResponse;
import app.company.api.room.BOUpdateRoomRequest;
import app.company.api.room.RoomViewStatus;
import app.company.domain.Company;
import app.reservation.api.BOReservationWebService;
import app.reservation.api.reservation.BOSearchReservationRequest;
import app.reservation.api.reservation.BOSearchReservationResponse;
import app.reservation.api.reservation.ReservationViewStatus;
import app.room.domain.Room;
import app.room.domain.RoomStatus;
import core.framework.db.Query;
import core.framework.db.Repository;
import core.framework.inject.Inject;
import core.framework.web.exception.ConflictException;
import core.framework.web.exception.ForbiddenException;
import core.framework.web.exception.NotFoundException;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import java.util.OptionalLong;
import java.util.stream.Collectors;

/**
 * @author joseph
 */
public class BORoomService {
    @Inject
    private Repository<Room> roomRepository;

    @Inject
    private Repository<Company> companyRepository;

    @Inject
    private BOReservationWebService boReservationWebService;

    public BOCreateRoomResponse create(BOCreateRoomRequest request) {
        Company company = companyRepository.get(request.companyId).orElseThrow(() ->
            new NotFoundException("Company not found, id= " + request.companyId));
        if (!company.adminId.equals(request.adminId))
            throw new ForbiddenException("You have no access" + request.adminId);
        Optional<Room> checkRoom = roomRepository.selectOne("name = ? and company_id = ?", request.roomName, request.companyId);
        if (checkRoom.isPresent())
            throw new ConflictException("This room name is already in use , name= " + request.roomName);
        Room room = new Room();
        room.companyId = request.companyId;
        room.name = request.roomName;
        room.adminId = request.adminId;
        room.status = RoomStatus.OPEN;
        room.updateTime = ZonedDateTime.now();
        OptionalLong insert = roomRepository.insert(room);
        if (insert.isPresent())
            room.id = (int) insert.getAsLong();
        return boCreateRoomResponse(room);
    }

    public BOSearchRoomResponse search(BOSearchRoomRequest request) {
        Query<Room> query = roomRepository.select();
        query.skip(request.skip);
        query.limit(request.limit);
        query.where("admin_id = ?", request.adminId);
        if (request.companyId != null) {
            if (!companyRepository.get(request.companyId).orElseThrow().adminId.equals(request.adminId))
                throw new ForbiddenException("You have no access");
            query.where("company_id = ?", request.companyId);
        }
        if (request.status != null)
            query.where("status = ?", request.status);
        BOSearchRoomResponse response = new BOSearchRoomResponse();
        response.total = query.count();
        response.rooms = query.fetch().stream().map(this::boSearchRoomView).collect(Collectors.toList());
        return response;
    }

    public void delete(Integer id, BODeleteRoomRequest request) {
        Room room = roomRepository.get(id).orElseThrow(() -> new NotFoundException("room not found, id= " + id));
        if (!room.adminId.equals(request.adminId)) {
            throw new ForbiddenException("You have no access" + request.adminId);
        } else {
            if (room.status != RoomStatus.CLOSED)
                throw new ConflictException("Room should be close before you delete, id= " + id);
            roomRepository.delete(id);
        }
    }

    public void update(Integer id, BOUpdateRoomRequest request) {
        Room room = roomRepository.get(id).orElseThrow(() -> new NotFoundException("room not found, id= " + id));
        if (!room.adminId.equals(request.adminId))
            throw new ForbiddenException("You have no access" + request.adminId);
        if (request.status != RoomViewStatus.CLOSE) {
            room.status = RoomStatus.valueOf(request.status.name());
        } else {
            BOSearchReservationRequest searchReservationRequest = new BOSearchReservationRequest();
            searchReservationRequest.roomId = id;
            searchReservationRequest.adminId = request.adminId;
            searchReservationRequest.companyId = room.companyId;
            List<BOSearchReservationResponse.BOSearchReservationView> reservations = boReservationWebService.search(searchReservationRequest).reservations;
            List<BOSearchReservationResponse.BOSearchReservationView> filterReservations = reservations.stream().filter(reservation ->
                reservation.status == ReservationViewStatus.RESERVED).collect(Collectors.toList());
            if (!filterReservations.isEmpty())
                room.status = RoomStatus.UNABLE;
            else
                room.status = RoomStatus.CLOSED;
        }
        room.updateTime = ZonedDateTime.now();
        roomRepository.partialUpdate(room);
    }

    private BOCreateRoomResponse boCreateRoomResponse(Room room) {
        BOCreateRoomResponse createRoomResponse = new BOCreateRoomResponse();
        createRoomResponse.companyId = room.companyId;
        createRoomResponse.roomId = room.id;
        createRoomResponse.roomName = room.name;
        createRoomResponse.updateTime = room.updateTime;
        return createRoomResponse;
    }

    private BOSearchRoomResponse.BOSearchRoomView boSearchRoomView(Room room) {
        BOSearchRoomResponse.BOSearchRoomView boSearchRoomView = new BOSearchRoomResponse.BOSearchRoomView();
        boSearchRoomView.roomName = room.name;
        boSearchRoomView.roomId = room.id;
        boSearchRoomView.companyId = room.companyId;
        boSearchRoomView.status = RoomViewStatus.valueOf(room.status.name());
        return boSearchRoomView;
    }
}
