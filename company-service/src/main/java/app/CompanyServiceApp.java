package app;

import app.room.job.AutoCloseRoomJob;
import core.framework.module.App;
import core.framework.module.SchedulerConfig;
import core.framework.module.SystemModule;

import java.time.LocalTime;

/**
 * @author joseph
 */
public class CompanyServiceApp extends App {
    @Override
    protected void initialize() {
        load(new SystemModule("sys.properties"));
        loadProperties("app.properties");
        load(new CompanyModule());
        load(new EmployeeModule());
        load(new RoomModule());

        SchedulerConfig schedulerConfig = schedule();
        schedulerConfig.dailyAt("auto-close-room", bind(AutoCloseRoomJob.class), LocalTime.MAX);
        //schedulerConfig.fixedRate("auto-close-room",bind(AutoCloseRoomJob.class), Duration.ofMinutes(1));
    }
}
