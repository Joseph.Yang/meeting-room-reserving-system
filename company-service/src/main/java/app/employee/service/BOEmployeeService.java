package app.employee.service;

import app.company.api.employee.BODeleteEmployeeRequest;
import app.company.api.employee.BOResetPasswordRequest;
import app.company.api.employee.BOSearchEmployeeRequest;
import app.company.api.employee.BOSearchEmployeeResponse;
import app.company.api.employee.BOUpdateEmployeeRequest;
import app.company.api.employee.EmployeeViewStatus;
import app.company.domain.Company;
import app.employee.domain.Employee;
import app.employee.domain.EmployeeStatus;
import app.reservation.api.ReservationWebService;
import app.reservation.api.reservation.ReservationViewStatus;
import app.reservation.api.reservation.SearchEmployeeReservationRequest;
import app.reservation.api.reservation.SearchEmployeeReservationsResponse;
import core.framework.db.Query;
import core.framework.db.Repository;
import core.framework.inject.Inject;
import core.framework.util.Strings;
import core.framework.web.exception.ConflictException;
import core.framework.web.exception.ForbiddenException;
import core.framework.web.exception.NotFoundException;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author joseph
 */
public class BOEmployeeService {
    @Inject
    private Repository<Employee> employeeRepository;

    @Inject
    private Repository<Company> companyRepository;

    @Inject
    private ReservationWebService reservationWebService;

    public void update(Integer id, BOUpdateEmployeeRequest request) {
        Employee employee = employeeRepository.get(id).orElseThrow(() -> new NotFoundException("Employee not found, id= " + id));
        Company company = companyRepository.get(employee.companyId).orElseThrow(() ->
            new NotFoundException("Company not found, id= " + employee.companyId));
        if (!company.adminId.equals(request.adminId))
            throw new ForbiddenException("You have no access" + request.adminId);
        if (request.status == EmployeeViewStatus.ACTIVE) {
            employee.status = EmployeeStatus.ACTIVE;
        } else {
            SearchEmployeeReservationRequest searchEmployeeReservationRequest = new SearchEmployeeReservationRequest();
            searchEmployeeReservationRequest.companyId = company.id;
            searchEmployeeReservationRequest.status = ReservationViewStatus.RESERVED;
            List<SearchEmployeeReservationsResponse.SearchReservationView> reservations
                = reservationWebService.searchEmployeeReservations(employee.id, searchEmployeeReservationRequest).reservations;
            if (!reservations.isEmpty())
                throw new ConflictException("This employee status can't be inactive because of outstanding reservation , id= " + id);
            employee.status = EmployeeStatus.INACTIVE;
        }
        employee.updateTime = ZonedDateTime.now();
        employeeRepository.partialUpdate(employee);
    }

    public void resetPassword(Integer id, BOResetPasswordRequest request) {
        Employee employee = employeeRepository.get(id).orElseThrow(() -> new NotFoundException("Employee not found, id= " + id));
        Company company = companyRepository.get(employee.companyId).orElseThrow(() ->
            new NotFoundException("Company not found, id= " + employee.companyId));
        if (!company.adminId.equals(request.adminId))
            throw new ForbiddenException("You have no access" + request.adminId);
        employee.password = "123";
        employee.updateTime = ZonedDateTime.now();
        employeeRepository.partialUpdate(employee);
    }

    public BOSearchEmployeeResponse search(BOSearchEmployeeRequest request) {
        Company company = companyRepository.get(request.companyId).orElseThrow(() ->
            new NotFoundException("Company not found, id= " + request.companyId));
        if (!company.adminId.equals(request.adminId))
            throw new ForbiddenException("You have no access" + request.adminId);
        Query<Employee> query = employeeRepository.select();
        query.skip(request.skip);
        query.limit(request.limit);
        if (!Strings.isBlank(request.employeeName))
            query.where("name LIKE ?", Strings.format("%{}%", request.employeeName));
        if (request.companyId != null)
            query.where("company_id = ?", request.companyId);
        BOSearchEmployeeResponse response = new BOSearchEmployeeResponse();
        response.employees = query.fetch().stream().map(this::boSearchEmployeeView).collect(Collectors.toList());
        response.total = query.count();
        return response;
    }

    public void delete(Integer id, BODeleteEmployeeRequest request) {
        Employee employee = employeeRepository.get(id).orElseThrow(() -> new NotFoundException("Employee not found, id= " + id));
        Company company = companyRepository.get(employee.companyId).orElseThrow(() ->
            new NotFoundException("Company not found, id= " + employee.companyId));
        if (!company.adminId.equals(request.adminId))
            throw new ForbiddenException("You have no access" + request.adminId);
        SearchEmployeeReservationRequest searchEmployeeReservationRequest = new SearchEmployeeReservationRequest();
        searchEmployeeReservationRequest.companyId = company.id;
        searchEmployeeReservationRequest.status = ReservationViewStatus.RESERVED;
        List<SearchEmployeeReservationsResponse.SearchReservationView> reservations
            = reservationWebService.searchEmployeeReservations(id, searchEmployeeReservationRequest).reservations;
        if (!reservations.isEmpty())
            throw new ConflictException("This employee status can't be delete because of outstanding reservation , id= " + id);
        employeeRepository.delete(id);
    }

    private BOSearchEmployeeResponse.BOSearchEmployeeView boSearchEmployeeView(Employee employee) {
        BOSearchEmployeeResponse.BOSearchEmployeeView boSearchEmployeeView = new BOSearchEmployeeResponse.BOSearchEmployeeView();
        boSearchEmployeeView.companyId = employee.companyId;
        boSearchEmployeeView.employeeId = employee.id;
        boSearchEmployeeView.employeeName = employee.name;
        boSearchEmployeeView.password = employee.password;
        boSearchEmployeeView.email = employee.email;
        boSearchEmployeeView.updateTime = employee.updateTime;
        boSearchEmployeeView.status = EmployeeViewStatus.valueOf(employee.status.name());
        return boSearchEmployeeView;
    }
}
