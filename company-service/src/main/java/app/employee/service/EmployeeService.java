package app.employee.service;

import app.company.api.employee.ChangePasswordRequest;
import app.company.api.employee.CreateEmployeeRequest;
import app.company.api.employee.CreateEmployeeResponse;
import app.company.api.employee.EmployeeLoginRequest;
import app.company.api.employee.EmployeeLoginResponse;
import app.company.api.employee.EmployeeView;
import app.company.api.employee.UpdateEmployeeRequest;
import app.company.domain.Company;
import app.employee.domain.Employee;
import app.employee.domain.EmployeeStatus;
import core.framework.db.Repository;
import core.framework.inject.Inject;
import core.framework.util.Strings;
import core.framework.web.exception.ConflictException;
import core.framework.web.exception.ForbiddenException;
import core.framework.web.exception.NotFoundException;

import java.time.ZonedDateTime;
import java.util.Optional;

/**
 * @author joseph
 */
public class EmployeeService {
    @Inject
    private Repository<Employee> employeeRepository;

    @Inject
    private Repository<Company> companyRepository;

    public EmployeeView get(Integer companyId, Integer employeeId) {
        Employee employee = employeeRepository.get(employeeId).orElseThrow(() ->
            new NotFoundException("Employee not found, id= " + employeeId));
        if (!employee.companyId.equals(companyId))
            throw new ForbiddenException("You have no access");
        return employeeView(employee);
    }

    public CreateEmployeeResponse create(Integer id, CreateEmployeeRequest request) {
        companyRepository.get(id).orElseThrow(() ->
            new NotFoundException("Company not found, id= " + id));
        Optional<Employee> existingEmployee
            = employeeRepository.selectOne("email = ?", request.email);
        if (existingEmployee.isPresent())
            throw new ConflictException("This email already exists, email= " + request.email);
        Employee employee = new Employee();
        employee.companyId = id;
        employee.name = request.employeeName;
        employee.password = request.password;
        employee.email = request.email;
        employee.updateTime = ZonedDateTime.now();
        employee.status = EmployeeStatus.INACTIVE;
        long employeeId = employeeRepository.insert(employee).orElseThrow();

        CreateEmployeeResponse response = new CreateEmployeeResponse();
        response.companyId = id;
        response.employeeId = (int) employeeId;
        response.employeeName = request.employeeName;
        response.email = request.email;
        return response;

    }

    public EmployeeLoginResponse login(EmployeeLoginRequest request) {
        Optional<Employee> employee = employeeRepository.selectOne("email = ?", request.email);
        if (employee.isEmpty())
            throw new NotFoundException("Employee not found, email= " + request.email);
        Employee loginEmployee = employee.get();
        if (loginEmployee.password.equals(request.password)) {
            if (loginEmployee.status == EmployeeStatus.ACTIVE) {
                EmployeeLoginResponse employeeLoginResponse = new EmployeeLoginResponse();
                employeeLoginResponse.employeeName = loginEmployee.name;
                employeeLoginResponse.employeeId = loginEmployee.id;
                employeeLoginResponse.companyId = loginEmployee.companyId;
                employeeLoginResponse.email = loginEmployee.email;
                return employeeLoginResponse;
            } else
                throw new ForbiddenException("This employee not be allowed");
        } else {
            throw new ForbiddenException("Employee email or password is wrong");
        }

    }

    public void update(Integer companyId, Integer employeeId, UpdateEmployeeRequest request) {
        Employee employee = employeeRepository.get(employeeId).orElseThrow(() -> new NotFoundException("Employee not found, id= " + employeeId));
        if (!companyId.equals(employee.companyId))
            throw new ForbiddenException("You have no access");
        if (!employee.password.equals(request.checkedPassword))
            throw new ForbiddenException("Please input true password");
        if (!Strings.isBlank(request.employeeName))
            employee.name = request.employeeName;
        if (!Strings.isBlank(request.email)) {
            Optional<Employee> existingEmployee
                = employeeRepository.selectOne("email = ?", request.email);
            if (existingEmployee.isPresent())
                throw new ConflictException("This email already exists, email= " + request.email);
            employee.email = request.email;
        }
        employee.updateTime = ZonedDateTime.now();
        employeeRepository.partialUpdate(employee);
    }

    public void changePassword(Integer companyId, Integer employeeId, ChangePasswordRequest request) {
        Employee employee = employeeRepository.get(employeeId).orElseThrow(() -> new NotFoundException("Employee not found, id= " + employeeId));
        if (!companyId.equals(employee.companyId))
            throw new ForbiddenException("You have no access");
        if (!employee.password.equals(request.oldPassword))
            throw new ForbiddenException("Please input true password");
        employee.password = request.newPassword;
        employee.updateTime = ZonedDateTime.now();
        employeeRepository.partialUpdate(employee);
    }

    private EmployeeView employeeView(Employee employee) {
        EmployeeView employeeView = new EmployeeView();
        employeeView.employeeId = employee.id;
        employeeView.employeeName = employee.name;
        employeeView.email = employee.email;
        employeeView.companyId = employee.companyId;
        return employeeView;
    }
}
