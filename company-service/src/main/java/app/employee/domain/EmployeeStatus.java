package app.employee.domain;

import core.framework.db.DBEnumValue;

/**
 * @author joseph
 */
public enum EmployeeStatus {
    @DBEnumValue("ACTIVE")
    ACTIVE,
    @DBEnumValue("INACTIVE")
    INACTIVE
}
