package app.employee.domain;

import core.framework.api.validate.NotBlank;
import core.framework.api.validate.NotNull;
import core.framework.db.Column;
import core.framework.db.PrimaryKey;
import core.framework.db.Table;

import java.time.ZonedDateTime;

/**
 * @author joseph
 */
@Table(name = "employees")
public class Employee {
    @PrimaryKey(autoIncrement = true)
    @Column(name = "id")
    public Integer id;

    @NotNull
    @NotBlank
    @Column(name = "name")
    public String name;

    @NotNull
    @NotBlank
    @Column(name = "password")
    public String password;

    @NotNull
    @Column(name = "company_id")
    public Integer companyId;

    @NotNull
    @NotBlank
    @Column(name = "email")
    public String email;

    @NotNull
    @Column(name = "status")
    public EmployeeStatus status;

    @NotNull
    @Column(name = "update_time")
    public ZonedDateTime updateTime;
}
