package app.employee.web;

import app.company.api.BOEmployeeWebService;
import app.company.api.employee.BODeleteEmployeeRequest;
import app.company.api.employee.BOResetPasswordRequest;
import app.company.api.employee.BOSearchEmployeeRequest;
import app.company.api.employee.BOSearchEmployeeResponse;
import app.company.api.employee.BOUpdateEmployeeRequest;
import app.employee.service.BOEmployeeService;
import core.framework.inject.Inject;

/**
 * @author joseph
 */
public class BOEmployeeWebServiceImpl implements BOEmployeeWebService {
    @Inject
    private BOEmployeeService boEmployeeService;

    @Override
    public void update(Integer id, BOUpdateEmployeeRequest request) {
        boEmployeeService.update(id, request);
    }

    @Override
    public void resetPassword(Integer id, BOResetPasswordRequest request) {
        boEmployeeService.resetPassword(id, request);
    }

    @Override
    public BOSearchEmployeeResponse search(BOSearchEmployeeRequest request) {
        return boEmployeeService.search(request);
    }

    @Override
    public void delete(Integer id, BODeleteEmployeeRequest request) {
        boEmployeeService.delete(id, request);
    }
}
