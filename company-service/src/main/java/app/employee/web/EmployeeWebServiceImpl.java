package app.employee.web;

import app.company.api.EmployeeWebService;
import app.company.api.employee.CreateEmployeeRequest;
import app.company.api.employee.CreateEmployeeResponse;
import app.company.api.employee.EmployeeLoginRequest;
import app.company.api.employee.EmployeeLoginResponse;
import app.company.api.employee.EmployeeView;
import app.company.api.employee.ChangePasswordRequest;
import app.company.api.employee.UpdateEmployeeRequest;
import app.employee.service.EmployeeService;
import core.framework.inject.Inject;

/**
 * @author joseph
 */
public class EmployeeWebServiceImpl implements EmployeeWebService {
    @Inject
    private EmployeeService employeeService;

    @Override
    public EmployeeView get(Integer companyId, Integer employeeId) {
        return employeeService.get(companyId, employeeId);
    }

    @Override
    public CreateEmployeeResponse create(Integer id, CreateEmployeeRequest request) {
        return employeeService.create(id, request);
    }

    @Override
    public void update(Integer companyId, Integer employeeId, UpdateEmployeeRequest request) {
        employeeService.update(companyId, employeeId, request);
    }

    @Override
    public void changePassword(Integer companyId, Integer employeeId, ChangePasswordRequest request) {
        employeeService.changePassword(companyId, employeeId, request);
    }

    @Override
    public EmployeeLoginResponse login(EmployeeLoginRequest request) {
        return employeeService.login(request);
    }
}
