package app.company.domain;

import core.framework.api.validate.NotBlank;
import core.framework.api.validate.NotNull;
import core.framework.db.Column;
import core.framework.db.PrimaryKey;
import core.framework.db.Table;

import java.time.ZonedDateTime;

/**
 * @author joseph
 */
@Table(name = "companies")
public class Company {
    @PrimaryKey(autoIncrement = true)
    @Column(name = "id")
    public Integer id;

    @NotNull
    @NotBlank
    @Column(name = "name")
    public String name;

    @NotNull
    @NotBlank
    @Column(name = "address")
    public String address;

    @NotNull
    @Column(name = "create_time")
    public ZonedDateTime createTime;

    @NotNull
    @Column(name = "update_time")
    public ZonedDateTime updateTime;

    @NotNull
    @Column(name = "admin_id")
    public String adminId;
}
