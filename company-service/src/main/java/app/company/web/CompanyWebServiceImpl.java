package app.company.web;

import app.company.api.CompanyWebService;
import app.company.api.company.CompanyView;
import app.company.api.company.SearchCompanyRequest;
import app.company.api.company.SearchCompanyResponse;
import app.company.service.CompanyService;
import core.framework.inject.Inject;

/**
 * @author joseph
 */
public class CompanyWebServiceImpl implements CompanyWebService {
    @Inject
    private CompanyService companyService;


    @Override
    public CompanyView get(Integer id) {
        return companyService.get(id);
    }

    @Override
    public SearchCompanyResponse search(SearchCompanyRequest request) {
        return companyService.search(request);
    }
}
