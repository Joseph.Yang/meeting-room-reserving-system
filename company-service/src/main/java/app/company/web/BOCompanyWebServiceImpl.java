package app.company.web;

import app.company.api.BOCompanyWebService;
import app.company.api.company.BOCreateCompanyRequest;
import app.company.api.company.BOCreateCompanyResponse;
import app.company.api.company.BODeleteCompanyRequest;
import app.company.api.company.BOSearchCompanyRequest;
import app.company.api.company.BOSearchCompanyResponse;
import app.company.api.company.BOUpdateCompanyRequest;
import app.company.service.BOCompanyService;
import core.framework.inject.Inject;

/**
 * @author joseph
 */
public class BOCompanyWebServiceImpl implements BOCompanyWebService {
    @Inject
    private BOCompanyService boCompanyService;

    @Override
    public BOCreateCompanyResponse create(BOCreateCompanyRequest request) {
        return boCompanyService.create(request);
    }

    @Override
    public BOSearchCompanyResponse search(BOSearchCompanyRequest request) {
        return boCompanyService.search(request);
    }

    @Override
    public void delete(Integer id, BODeleteCompanyRequest request) {
        boCompanyService.delete(id, request);
    }

    @Override
    public void update(Integer id, BOUpdateCompanyRequest request) {
        boCompanyService.update(id, request);
    }

}
