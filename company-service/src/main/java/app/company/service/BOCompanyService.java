package app.company.service;

import app.company.api.company.BOCreateCompanyRequest;
import app.company.api.company.BOCreateCompanyResponse;
import app.company.api.company.BODeleteCompanyRequest;
import app.company.api.company.BOSearchCompanyRequest;
import app.company.api.company.BOSearchCompanyResponse;
import app.company.api.company.BOUpdateCompanyRequest;
import app.company.domain.Company;
import app.employee.domain.Employee;
import core.framework.db.Query;
import core.framework.db.Repository;
import core.framework.inject.Inject;
import core.framework.util.Strings;
import core.framework.web.exception.ConflictException;
import core.framework.web.exception.ForbiddenException;
import core.framework.web.exception.NotFoundException;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import java.util.OptionalLong;
import java.util.stream.Collectors;

/**
 * @author joseph
 */
public class BOCompanyService {
    @Inject
    private Repository<Company> companyRepository;

    @Inject
    private Repository<Employee> employeeRepository;

    public BOCreateCompanyResponse create(BOCreateCompanyRequest request) {
        Optional<Company> checkCompany = companyRepository.selectOne("name = ?", request.companyName);
        if (checkCompany.isPresent())
            throw new ConflictException("This companyName is already in use , name= " + request.companyName);
        Company company = new Company();
        company.name = request.companyName;
        company.address = request.companyAddress;
        company.createTime = ZonedDateTime.now();
        company.updateTime = ZonedDateTime.now();
        company.adminId = request.adminId;
        OptionalLong insert = companyRepository.insert(company);
        if (insert.isPresent())
            company.id = (int) insert.getAsLong();
        return boCreateCompanyResponse(company);
    }

    public void delete(Integer id, BODeleteCompanyRequest request) {
        Company company = companyRepository.get(id).orElseThrow(() -> new NotFoundException("company not found, id= " + id));
        if (!company.adminId.equals(request.adminId))
            throw new ForbiddenException("You have no access" + request.adminId);
        List<Employee> employees = employeeRepository.select("company_id = ?", id);
        if (!employees.isEmpty())
            throw new ConflictException("You can not delete because you have employee in company, id= " + id);
        companyRepository.delete(id);
    }

    public BOSearchCompanyResponse search(BOSearchCompanyRequest request) {
        Query<Company> query = companyRepository.select();
        BOSearchCompanyResponse response = new BOSearchCompanyResponse();
        query.limit(request.limit);
        query.skip(request.skip);
        if (!Strings.isBlank(request.adminId))
            query.where("admin_id = ?", request.adminId);
        response.companies = query.fetch().stream().map(this::boSearchCompanyView).collect(Collectors.toList());
        response.total = query.count();
        return response;
    }

    public void update(Integer companyId, BOUpdateCompanyRequest request) {
        Company company = companyRepository.get(companyId).orElseThrow(() -> new NotFoundException("company not found, id= " + companyId));
        if (!request.adminId.equals(company.adminId))
            throw new ForbiddenException("You have no access" + request.adminId);
        if (!Strings.isBlank(request.companyAddress))
            company.address = request.companyAddress;
        if (!Strings.isBlank(request.companyName))
            company.name = request.companyName;
        company.updateTime = ZonedDateTime.now();
        companyRepository.partialUpdate(company);
    }

    private BOSearchCompanyResponse.BOSearchCompanyView boSearchCompanyView(Company company) {
        BOSearchCompanyResponse.BOSearchCompanyView boSearchCompanyView = new BOSearchCompanyResponse.BOSearchCompanyView();
        boSearchCompanyView.companyName = company.name;
        boSearchCompanyView.companyId = company.id;
        boSearchCompanyView.createTime = company.createTime;
        boSearchCompanyView.updateTime = company.updateTime;
        boSearchCompanyView.address = company.address;
        boSearchCompanyView.adminId = company.adminId;
        return boSearchCompanyView;
    }

    private BOCreateCompanyResponse boCreateCompanyResponse(Company company) {
        BOCreateCompanyResponse boCreateCompanyResponse = new BOCreateCompanyResponse();
        boCreateCompanyResponse.companyName = company.name;
        boCreateCompanyResponse.companyId = company.id;
        boCreateCompanyResponse.createTime = company.createTime;
        boCreateCompanyResponse.updateTime = company.updateTime;
        boCreateCompanyResponse.address = company.address;
        return boCreateCompanyResponse;
    }
}
