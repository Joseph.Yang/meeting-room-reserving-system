package app.company.service;

import app.company.api.company.CompanyView;
import app.company.api.company.SearchCompanyRequest;
import app.company.api.company.SearchCompanyResponse;
import app.company.domain.Company;
import core.framework.db.Query;
import core.framework.db.Repository;
import core.framework.inject.Inject;
import core.framework.util.Strings;
import core.framework.web.exception.NotFoundException;

import java.util.stream.Collectors;

/**
 * @author joseph
 */
public class CompanyService {
    @Inject
    private Repository<Company> companyRepository;

    public CompanyView get(Integer id) {
        Company company = companyRepository.get(id).orElseThrow(() -> new NotFoundException("company not found, id= " + id));
        return companyView(company);
    }

    public SearchCompanyResponse search(SearchCompanyRequest request) {
        Query<Company> query = companyRepository.select();
        SearchCompanyResponse response = new SearchCompanyResponse();
        query.skip(request.skip);
        query.limit(request.limit);
        if (!Strings.isBlank(request.companyName))
            query.where("name LIKE ?", Strings.format("%{}%", request.companyName));
        response.companies = query.fetch().stream().map(this::searchCompanyView).collect(Collectors.toList());
        response.total = query.count();
        return response;
    }

    private SearchCompanyResponse.SearchCompanyView searchCompanyView(Company company) {
        SearchCompanyResponse.SearchCompanyView searchCompanyView = new SearchCompanyResponse.SearchCompanyView();
        searchCompanyView.companyName = company.name;
        searchCompanyView.companyId = company.id;
        searchCompanyView.address = company.address;
        searchCompanyView.createTime = company.createTime;
        return searchCompanyView;
    }

    private CompanyView companyView(Company company) {
        CompanyView companyView = new CompanyView();
        companyView.companyId = company.id;
        companyView.companyName = company.name;
        companyView.address = company.address;
        companyView.createTime = company.createTime;
        companyView.updateTime = company.updateTime;
        companyView.adminId = company.adminId;
        return companyView;
    }
}

