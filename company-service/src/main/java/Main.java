import app.CompanyServiceApp;

/**
 * @author joseph
 */
public class Main {
    public static void main(String[] args) {
        new CompanyServiceApp().start();
    }
}
