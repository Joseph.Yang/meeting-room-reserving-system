import app.ReservationServiceApp;

/**
 * @author joseph
 */
public class Main {
    public static void main(String[] args) {
        new ReservationServiceApp().start();
    }
}
