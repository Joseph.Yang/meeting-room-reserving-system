package app;

import app.company.api.CompanyWebService;
import app.company.api.EmployeeWebService;
import app.company.api.RoomWebService;
import app.reservation.api.BOReservationWebService;
import app.reservation.api.ReservationWebService;
import app.reservation.domain.Reservation;
import app.reservation.service.BOReservationService;
import app.reservation.service.ReservationService;
import app.reservation.web.BOReservationWebServiceImpl;
import app.reservation.web.ReservationWebServiceImpl;
import core.framework.module.Module;
import core.framework.mongo.module.MongoConfig;

/**
 * @author joseph
 */
public class ReservationModule extends Module {
    @Override
    protected void initialize() {
        MongoConfig config = config(MongoConfig.class);
        config.uri(requiredProperty("sys.mongo.uri"));
        config.collection(Reservation.class);

        api().client(RoomWebService.class, requiredProperty("app.companyWebService.url"));
        api().client(EmployeeWebService.class, requiredProperty("app.companyWebService.url"));
        api().client(CompanyWebService.class, requiredProperty("app.companyWebService.url"));

        bind(ReservationService.class);
        bind(BOReservationService.class);
        api().service(ReservationWebService.class, bind(ReservationWebServiceImpl.class));
        api().service(BOReservationWebService.class, bind(BOReservationWebServiceImpl.class));

    }
}
