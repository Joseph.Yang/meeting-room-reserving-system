package app.reservation.web;

import app.reservation.api.BOReservationWebService;
import app.reservation.api.reservation.BOSearchReservationRequest;
import app.reservation.api.reservation.BOSearchReservationResponse;
import app.reservation.api.reservation.BOCancelReservationRequest;
import app.reservation.service.BOReservationService;
import core.framework.inject.Inject;

/**
 * @author joseph
 */
public class BOReservationWebServiceImpl implements BOReservationWebService {
    @Inject
    private BOReservationService reservationService;

    @Override
    public BOSearchReservationResponse search(BOSearchReservationRequest request) {
        return reservationService.search(request);
    }

    @Override
    public void delete(String id) {
        reservationService.delete(id);
    }

    @Override
    public void cancel(String id, BOCancelReservationRequest request) {
        reservationService.cancel(id, request);
    }
}
