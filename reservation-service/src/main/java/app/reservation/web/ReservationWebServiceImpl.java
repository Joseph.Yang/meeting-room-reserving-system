package app.reservation.web;

import app.reservation.api.ReservationWebService;
import app.reservation.api.reservation.CancelReservationRequest;
import app.reservation.api.reservation.CreateReservationRequest;
import app.reservation.api.reservation.CreateReservationResponse;
import app.reservation.api.reservation.SearchEmployeeReservationRequest;
import app.reservation.api.reservation.SearchEmployeeReservationsResponse;
import app.reservation.api.reservation.SearchFreeRangeEndTimeRequest;
import app.reservation.api.reservation.SearchFreeRangeEndTimeResponse;
import app.reservation.api.reservation.SearchReservationRequest;
import app.reservation.api.reservation.SearchReservationResponse;
import app.reservation.service.ReservationService;
import core.framework.inject.Inject;
import core.framework.log.ActionLogContext;

/**
 * @author joseph
 */
public class ReservationWebServiceImpl implements ReservationWebService {
    @Inject
    private ReservationService reservationService;

    @Override
    public CreateReservationResponse create(CreateReservationRequest request) {
        return reservationService.create(request);
    }

    @Override
    public SearchReservationResponse search(SearchReservationRequest request) {
        return reservationService.search(request);
    }

    @Override
    public SearchEmployeeReservationsResponse searchEmployeeReservations(Integer id, SearchEmployeeReservationRequest request) {
        return reservationService.searchEmployeeReservations(id, request);
    }

    @Override
    public SearchFreeRangeEndTimeResponse searchFreeRangeEndTime(SearchFreeRangeEndTimeRequest request) {
        return reservationService.searchFreeRangeEndByRangeBegin(request);
    }

    @Override
    public void cancel(String id, CancelReservationRequest request) {
        ActionLogContext.put("id", id);
        reservationService.cancel(id, request);
    }

    @Override
    public void complete(String id) {
        reservationService.complete(id);
    }
}
