package app.reservation.domain;

import core.framework.mongo.MongoEnumValue;

/**
 * @author joseph
 */
public enum ReservationStatus {
    @MongoEnumValue("CANCELED")
    CANCELED,

    @MongoEnumValue("RESERVED")
    RESERVED,

    @MongoEnumValue("COMPLETED")
    COMPLETED
}
