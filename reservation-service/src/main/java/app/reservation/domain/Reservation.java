package app.reservation.domain;

import core.framework.api.validate.NotNull;
import core.framework.mongo.Collection;
import core.framework.mongo.Field;
import core.framework.mongo.Id;

import java.time.ZonedDateTime;

/**
 * @author joseph
 */
@Collection(name = "reservations")
public class Reservation {
    @Id
    @Field(name = "id")
    public String id;

    @NotNull
    @Field(name = "start_time")
    public ZonedDateTime startTime;

    @NotNull
    @Field(name = "end_time")
    public ZonedDateTime endTime;

    @NotNull
    @Field(name = "status")
    public ReservationStatus status;

    @NotNull
    @Field(name = "employee")
    public Employee employee;

    @NotNull
    @Field(name = "company")
    public Company company;

    @NotNull
    @Field(name = "room")
    public Room room;
}
