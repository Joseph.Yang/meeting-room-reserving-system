package app.reservation.domain;

import core.framework.mongo.Field;

/**
 * @author joseph
 */
public class Room {
    @Field(name = "id")
    public Integer id;

    @Field(name = "name")
    public String name;
}
