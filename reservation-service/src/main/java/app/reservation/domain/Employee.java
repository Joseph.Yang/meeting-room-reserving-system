package app.reservation.domain;

import core.framework.mongo.Field;

/**
 * @author joseph
 */
public class Employee {
    @Field(name = "id")
    public Integer id;

    @Field(name = "name")
    public String name;

    @Field(name = "email")
    public String email;
}
