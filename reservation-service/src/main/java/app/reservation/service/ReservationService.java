package app.reservation.service;

import app.company.api.CompanyWebService;
import app.company.api.EmployeeWebService;
import app.company.api.RoomWebService;
import app.company.api.company.CompanyView;
import app.company.api.employee.EmployeeView;
import app.company.api.room.RoomView;
import app.reservation.api.kafka.CancelReservationMessage;
import app.reservation.api.kafka.CreateReservationMessage;
import app.reservation.api.reservation.CancelReservationRequest;
import app.reservation.api.reservation.CreateReservationRequest;
import app.reservation.api.reservation.CreateReservationResponse;
import app.reservation.api.reservation.ReservationViewStatus;
import app.reservation.api.reservation.SearchEmployeeReservationRequest;
import app.reservation.api.reservation.SearchEmployeeReservationsResponse;
import app.reservation.api.reservation.SearchFreeRangeEndTimeRequest;
import app.reservation.api.reservation.SearchFreeRangeEndTimeResponse;
import app.reservation.api.reservation.SearchReservationRequest;
import app.reservation.api.reservation.SearchReservationResponse;
import app.reservation.domain.Company;
import app.reservation.domain.Employee;
import app.reservation.domain.Reservation;
import app.reservation.domain.ReservationStatus;
import app.reservation.domain.Room;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Sorts;
import core.framework.inject.Inject;
import core.framework.kafka.MessagePublisher;
import core.framework.mongo.MongoCollection;
import core.framework.mongo.Query;
import core.framework.web.exception.ConflictException;
import core.framework.web.exception.ForbiddenException;
import core.framework.web.exception.NotFoundException;
import org.bson.conversions.Bson;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * @author joseph
 */
public class ReservationService {
    @Inject
    private MongoCollection<Reservation> mongoCollection;

    @Inject
    private CompanyWebService companyWebService;

    @Inject
    private RoomWebService roomWebService;

    @Inject
    private EmployeeWebService employeeWebService;

    @Inject
    private MessagePublisher<CreateReservationMessage> createReservationMessageMessagePublisher;

    @Inject
    private MessagePublisher<CancelReservationMessage> cancelReservationMessageMessagePublisher;

    public CreateReservationResponse create(CreateReservationRequest request) {
        EmployeeView employeeView = employeeWebService.get(request.companyId, request.employeeId);
        CompanyView companyView = companyWebService.get(request.companyId);
        RoomView roomView = roomWebService.get(request.companyId, request.roomId);
        if (!roomView.companyId.equals(request.companyId))
            throw new ForbiddenException("You have no access");
        if (!roomView.companyId.equals(employeeView.companyId))
            throw new ForbiddenException("You have no access");
        if (request.startTime.getDayOfYear() - ZonedDateTime.now().getDayOfYear() > 7)
            throw new ForbiddenException("Out of date range");
        int dateSubtraction = request.startTime.getDayOfYear() - ZonedDateTime.now().getDayOfYear();
        ZonedDateTime lastEndTime = getLastEndTime().minusDays(-dateSubtraction);
        if (request.endTime.compareTo(lastEndTime) > 0 || request.startTime.compareTo(request.endTime) > 0
            || request.startTime.compareTo(request.endTime) == 0 || request.startTime.compareTo(ZonedDateTime.now()) < 0
            || request.startTime.compareTo(ZonedDateTime.now().minusMinutes(-10)) < 0)
            throw new ForbiddenException("Please check the true time");
        if (isReserved(request.roomId, request.startTime, request.endTime))
            throw new ConflictException("This room is already reserved");
        Reservation reservation = new Reservation();
        reservation.id = UUID.randomUUID().toString();
        reservation.employee = new Employee();
        reservation.employee.id = request.employeeId;
        reservation.employee.name = employeeView.employeeName;
        reservation.employee.email = employeeView.email;
        reservation.company = new Company();
        reservation.company.id = request.companyId;
        reservation.company.name = companyView.companyName;
        reservation.room = new Room();
        reservation.room.id = roomView.roomId;
        reservation.room.name = roomView.roomName;
        reservation.startTime = request.startTime.minusDays(-dateSubtraction);
        reservation.endTime = request.endTime.minusDays(-dateSubtraction);
        reservation.status = ReservationStatus.RESERVED;
        mongoCollection.insert(reservation);

        CreateReservationMessage createReservationMessage = new CreateReservationMessage();
        createReservationMessage.employeeEmail = reservation.employee.email;
        createReservationMessage.notifyTime = reservation.startTime.minusMinutes(10);
        createReservationMessage.completeTime = reservation.endTime;
        createReservationMessage.reservationId = reservation.id;
        createReservationMessageMessagePublisher.publish(createReservationMessage);

        return createReservationResponse(reservation);
    }

    public SearchReservationResponse search(SearchReservationRequest request) {
        int dateSubtraction;
        if (request.startTime != null)
            dateSubtraction = request.startTime.getDayOfYear() - ZonedDateTime.now().getDayOfYear();
        else
            dateSubtraction = 0;
        if (dateSubtraction > 7 || dateSubtraction < 0)
            throw new ConflictException("Out of date range");
        roomWebService.get(request.companyId, request.roomId);
        ZonedDateTime firstStartTime = getFirstStartTime().minusDays(-dateSubtraction);
        ZonedDateTime lastEndTime = getLastEndTime().minusDays(-dateSubtraction);
        List<ZonedDateTime> freeStartTimes;
        Query query = new Query();
        Bson roomFilter = Filters.eq("room.id", request.roomId);
        freeStartTimes = searchFree(request.roomId, firstStartTime, lastEndTime);
        Bson statusFilter = Filters.eq("status", ReservationStatus.RESERVED);
        if (request.startTime != null)
            firstStartTime = request.startTime;
        if (request.endTime != null)
            lastEndTime = request.endTime;
        Bson startTimeFilter = Filters.gte("start_time", firstStartTime);
        Bson endTimeFilter = Filters.lte("end_time", lastEndTime);
        query.filter = Filters.and(roomFilter, statusFilter, startTimeFilter, endTimeFilter);
        SearchReservationResponse response = new SearchReservationResponse();
        response.total = (int) mongoCollection.count(query.filter);
        response.freeStartTimes = freeStartTimes;
        response.reservations = mongoCollection.find(query.filter).stream().map(this::searchReservationViewView).collect(Collectors.toList());
        return response;
    }

    public SearchEmployeeReservationsResponse searchEmployeeReservations(Integer id, SearchEmployeeReservationRequest request) {
        employeeWebService.get(request.companyId, id);
        Query query = new Query();
        Bson companyFilter = Filters.eq("company.id", request.companyId);
        Bson employeeFilter = Filters.eq("employee.id", id);
        if (request.status != null) {
            Bson statusFilter = Filters.eq("status", request.status.name());
            query.filter = Filters.and(companyFilter, employeeFilter, statusFilter);
        } else
            query.filter = Filters.and(companyFilter, employeeFilter);
        SearchEmployeeReservationsResponse response = new SearchEmployeeReservationsResponse();
        response.reservations = mongoCollection.find(query.filter).stream().map(this::searchEmployeeReservationView).collect(Collectors.toList());
        response.total = (int) mongoCollection.count(query.filter);
        return response;
    }

    public void cancel(String id, CancelReservationRequest request) {
        Reservation reservation = mongoCollection.get(id).orElseThrow(() ->
            new NotFoundException("Reservation not found, id= ", id));
        if (!reservation.employee.id.equals(request.employeeId))
            throw new ForbiddenException("This is not your reservation");
        if (reservation.status != ReservationStatus.RESERVED)
            throw new ForbiddenException("Only status in reserved");
        if (reservation.startTime.minusMinutes(10).compareTo(ZonedDateTime.now()) < 0)
            throw new ForbiddenException("It's past the cancellation time");
        reservation.status = ReservationStatus.CANCELED;
        mongoCollection.replace(reservation);

        CancelReservationMessage cancelReservationMessage = new CancelReservationMessage();
        cancelReservationMessage.employeeId = request.employeeId;
        cancelReservationMessage.reservationId = id;
        cancelReservationMessageMessagePublisher.publish(cancelReservationMessage);
    }

    public void complete(String id) {
        Reservation reservation = mongoCollection.get(id).orElseThrow(() ->
            new NotFoundException("Reservation not found, id= ", id));
        reservation.status = ReservationStatus.COMPLETED;
        mongoCollection.replace(reservation);
    }

    public SearchFreeRangeEndTimeResponse searchFreeRangeEndByRangeBegin(SearchFreeRangeEndTimeRequest request) {
        if (!roomWebService.get(request.companyId, request.roomId).companyId.equals(request.companyId))
            throw new ForbiddenException("You have no access");
        Query query = new Query();
        Bson roomFilter = Filters.eq("room.id", request.roomId);
        Bson startTimeFilter = Filters.gt("start_time", request.beginTime);
        Bson statusFilter = Filters.eq("status", ReservationViewStatus.RESERVED.name());
        int dateSubtraction = request.beginTime.getDayOfYear() - ZonedDateTime.now().getDayOfYear();
        Bson endTimeFilter = Filters.lte("end_time", getLastEndTime().minusDays(-dateSubtraction));
        query.filter = Filters.and(roomFilter, startTimeFilter, statusFilter, endTimeFilter);
        query.sort = Sorts.ascending("start_time");
        List<Reservation> reservations = mongoCollection.find(query.filter);
        //reservations.sort(Comparator.comparing(reservation -> reservation.startTime));
        ZonedDateTime rangeEndTime;
        if (!reservations.isEmpty()) {
            rangeEndTime = reservations.get(0).startTime;
        } else {
            rangeEndTime = getLastEndTime().minusDays(-dateSubtraction);
        }
        SearchFreeRangeEndTimeResponse searchFreeRangeEndTimeResponse = new SearchFreeRangeEndTimeResponse();
        searchFreeRangeEndTimeResponse.freeRangeEndTimes =
            searchFree(request.roomId, request.beginTime, rangeEndTime).stream().map(endTime ->
                ZonedDateTime.ofInstant(Instant.ofEpochSecond(endTime.toInstant().getEpochSecond() + 1800), ZoneId.systemDefault()))
                .collect(Collectors.toList());
        return searchFreeRangeEndTimeResponse;
    }

    private CreateReservationResponse createReservationResponse(Reservation reservation) {
        CreateReservationResponse createReservationResponse = new CreateReservationResponse();
        createReservationResponse.companyName = reservation.company.name;
        createReservationResponse.roomName = reservation.room.name;
        createReservationResponse.employeeName = reservation.employee.name;
        createReservationResponse.startTime = reservation.startTime;
        createReservationResponse.endTime = reservation.endTime;
        return createReservationResponse;
    }

    private SearchReservationResponse.SearchReservationView searchReservationViewView(Reservation reservation) {
        SearchReservationResponse.SearchReservationView searchReservationViewView = new SearchReservationResponse.SearchReservationView();
        searchReservationViewView.employeeName = reservation.employee.name;
        searchReservationViewView.roomName = reservation.room.name;
        searchReservationViewView.startTime = reservation.startTime;
        searchReservationViewView.endTime = reservation.endTime;
        searchReservationViewView.status = ReservationViewStatus.valueOf(reservation.status.name());
        return searchReservationViewView;
    }

    private SearchEmployeeReservationsResponse.SearchReservationView searchEmployeeReservationView(Reservation reservation) {
        SearchEmployeeReservationsResponse.SearchReservationView searchReservationView = new SearchEmployeeReservationsResponse.SearchReservationView();
        searchReservationView.roomName = reservation.room.name;
        searchReservationView.startTime = reservation.startTime;
        searchReservationView.endTime = reservation.endTime;
        searchReservationView.status = ReservationViewStatus.valueOf(reservation.status.name());
        return searchReservationView;
    }

    private boolean isReserved(Integer roomId, ZonedDateTime startTime, ZonedDateTime endTime) {
        Bson startTimeComparedToStart = Filters.lte("start_time", startTime);
        Bson startTimeComparedToEnd = Filters.lt("start_time", endTime);
        Bson endTimeComparedToStart = Filters.gt("end_time", startTime);
        Bson endTimeComparedToEnd = Filters.gte("end_time", endTime);
        Bson startTimeFilter = Filters.gt("start_time", startTime);
        Bson endTimeFilter = Filters.lt("end_time", endTime);
        Bson roomFilter = Filters.eq("room.id", roomId);
        Bson statusFilter = Filters.eq("status", ReservationStatus.RESERVED.name());
        List<Reservation> reservationsByStartTime = mongoCollection.find(Filters.and(startTimeComparedToStart, endTimeComparedToStart, roomFilter, statusFilter));
        List<Reservation> reservationsByEndTime = mongoCollection.find(Filters.and(startTimeComparedToEnd, endTimeComparedToEnd, roomFilter, statusFilter));
        List<Reservation> reservationsByStartAndEnd = mongoCollection.find(Filters.and(startTimeFilter, endTimeFilter, roomFilter, statusFilter));
        return !reservationsByStartTime.isEmpty() || !reservationsByEndTime.isEmpty() || !reservationsByStartAndEnd.isEmpty();

    }

    private List<ZonedDateTime> searchFree(Integer roomId, ZonedDateTime startTime, ZonedDateTime endTime) {
        List<ZonedDateTime> freeStartTimes = new ArrayList<>();
        long startMinutes = startTime.toInstant().getEpochSecond() / 60;

        ZonedDateTime rangeBegin = startTime;
        ZonedDateTime rangeEnd = calculateRangeEnd(rangeBegin);
        long endMinutes = endTime.toInstant().getEpochSecond() / 60;
        int rangNum = (int) (endMinutes - startMinutes) / 30;
        for (int i = 0; i < rangNum; i++) {
            if (isReserved(roomId, rangeBegin, rangeEnd)) {
                rangeBegin = calculateRangeEnd(rangeBegin);
                rangeEnd = calculateRangeEnd(rangeBegin);
            } else {
                freeStartTimes.add(rangeBegin);
                rangeBegin = calculateRangeEnd(rangeBegin);
                rangeEnd = calculateRangeEnd(rangeBegin);
            }
        }
        return freeStartTimes.stream().filter(freeStartTime -> freeStartTime.compareTo(ZonedDateTime.now().minusMinutes(-10)) > 0).collect(Collectors.toList());
    }

    private ZonedDateTime calculateRangeEnd(ZonedDateTime rangeBegin) {
        return ZonedDateTime.ofInstant(Instant.ofEpochSecond(rangeBegin.toInstant().getEpochSecond() + 1800), ZoneId.systemDefault());
    }

    private ZonedDateTime getLastEndTime() {
        return ZonedDateTime.of(LocalDateTime.of(LocalDate.now(), LocalTime.MIN), ZoneId.systemDefault()).minusDays(-1);
    }

    private ZonedDateTime getFirstStartTime() {
        return ZonedDateTime.of(LocalDateTime.of(LocalDate.now(), LocalTime.MIN), ZoneId.systemDefault()).minusHours(-9);
    }
}
