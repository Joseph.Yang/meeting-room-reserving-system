package app.reservation.service;

import app.company.api.CompanyWebService;
import app.company.api.RoomWebService;
import app.reservation.api.kafka.CancelReservationMessage;
import app.reservation.api.reservation.BOCancelReservationRequest;
import app.reservation.api.reservation.BOSearchReservationRequest;
import app.reservation.api.reservation.BOSearchReservationResponse;
import app.reservation.api.reservation.ReservationViewStatus;
import app.reservation.domain.Reservation;
import app.reservation.domain.ReservationStatus;
import com.mongodb.client.model.Filters;
import core.framework.inject.Inject;
import core.framework.kafka.MessagePublisher;
import core.framework.mongo.MongoCollection;
import core.framework.mongo.Query;
import core.framework.web.exception.ConflictException;
import core.framework.web.exception.ForbiddenException;
import core.framework.web.exception.NotFoundException;
import org.bson.conversions.Bson;

import java.util.stream.Collectors;

/**
 * @author joseph
 */
public class BOReservationService {
    @Inject
    private MongoCollection<Reservation> mongoCollection;

    @Inject
    private CompanyWebService companyWebService;

    @Inject
    private RoomWebService roomWebService;

    @Inject
    private MessagePublisher<CancelReservationMessage> cancelReservationMessageMessagePublisher;

    public void delete(String id) {
        Reservation reservation = mongoCollection.get(id).orElseThrow(() -> new NotFoundException("reservation not found, id= ", id));
        if (reservation.status != ReservationStatus.RESERVED)
            mongoCollection.delete(id);
        else
            throw new ConflictException("You can not delete reservation in reserved");
    }

    public BOSearchReservationResponse search(BOSearchReservationRequest request) {
        roomWebService.get(request.companyId, request.roomId);
        Query query = new Query();
        query.skip = request.skip;
        query.limit = request.limit;
        Bson roomFilter = Filters.eq("room.id", request.roomId);
        if (request.status != null) {
            Bson statusFilter = Filters.eq("status", ReservationStatus.valueOf(request.status.name()));
            query.filter = Filters.and(roomFilter, statusFilter);
        } else
            query.filter = Filters.and(roomFilter);
        BOSearchReservationResponse response = new BOSearchReservationResponse();
        response.total = (int) mongoCollection.count(query.filter);
        response.reservations = mongoCollection.find(query.filter).stream().map(this::searchReservationView).collect(Collectors.toList());
        return response;
    }

    public void cancel(String id, BOCancelReservationRequest request) {
        Reservation reservation = mongoCollection.get(id).orElseThrow(() -> new NotFoundException("reservation not found, id= ", id));
        if (!companyWebService.get(reservation.company.id).adminId.equals(request.adminId))
            throw new ForbiddenException("You have no access");
        reservation.status = ReservationStatus.CANCELED;
        mongoCollection.replace(reservation);

        CancelReservationMessage cancelReservationMessage = new CancelReservationMessage();
        cancelReservationMessage.reservationId = reservation.id;
        cancelReservationMessageMessagePublisher.publish(cancelReservationMessage);
    }

    private BOSearchReservationResponse.BOSearchReservationView searchReservationView(Reservation reservation) {
        BOSearchReservationResponse.BOSearchReservationView searchReservationView = new BOSearchReservationResponse.BOSearchReservationView();
        searchReservationView.employeeName = reservation.employee.name;
        searchReservationView.roomName = reservation.room.name;
        searchReservationView.startTime = reservation.startTime;
        searchReservationView.endTime = reservation.endTime;
        searchReservationView.status = ReservationViewStatus.valueOf(reservation.status.name());
        searchReservationView.companyName = reservation.company.name;
        return searchReservationView;
    }
}
