package app;

import app.reservation.api.kafka.CancelReservationMessage;
import app.reservation.api.kafka.CreateReservationMessage;
import core.framework.module.App;
import core.framework.module.KafkaConfig;
import core.framework.module.SystemModule;

/**
 * @author joseph
 */
public class ReservationServiceApp extends App {
    @Override
    protected void initialize() {
        load(new SystemModule("sys.properties"));
        loadProperties("app.properties");

        KafkaConfig kafkaConfig = kafka();
        kafkaConfig.publish("create-reservation", CreateReservationMessage.class);
        kafkaConfig.publish("cancel-reservation", CancelReservationMessage.class);

        load(new ReservationModule());
    }
}
