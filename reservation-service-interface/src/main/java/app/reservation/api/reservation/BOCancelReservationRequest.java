package app.reservation.api.reservation;

import core.framework.api.json.Property;
import core.framework.api.validate.NotBlank;
import core.framework.api.validate.NotNull;

/**
 * @author joseph
 */
public class BOCancelReservationRequest {
    @NotNull
    @NotBlank
    @Property(name = "admin_id")
    public String adminId;
}
