package app.reservation.api.reservation;

import core.framework.api.json.Property;

import java.time.ZonedDateTime;
import java.util.List;

/**
 * @author joseph
 */
public class SearchFreeRangeEndTimeResponse {
    @Property(name = "free_range_end_times")
    public List<ZonedDateTime> freeRangeEndTimes;
}
