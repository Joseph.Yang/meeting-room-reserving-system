package app.reservation.api.reservation;

import core.framework.api.json.Property;

/**
 * @author joseph
 */
public enum ReservationViewStatus {
    @Property(name = "CANCELED")
    CANCELED,

    @Property(name = "RESERVED")
    RESERVED,

    @Property(name = "COMPLETED")
    COMPLETED
}
