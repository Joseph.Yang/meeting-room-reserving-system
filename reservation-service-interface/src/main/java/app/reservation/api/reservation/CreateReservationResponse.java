package app.reservation.api.reservation;

import core.framework.api.json.Property;
import core.framework.api.validate.NotNull;

import java.time.ZonedDateTime;

/**
 * @author joseph
 */
public class CreateReservationResponse {
    @NotNull
    @Property(name = "company_name")
    public String companyName;

    @NotNull
    @Property(name = "room_name")
    public String roomName;

    @NotNull
    @Property(name = "employee_name")
    public String employeeName;

    @NotNull
    @Property(name = "start_time")
    public ZonedDateTime startTime;

    @NotNull
    @Property(name = "end_time")
    public ZonedDateTime endTime;
}
