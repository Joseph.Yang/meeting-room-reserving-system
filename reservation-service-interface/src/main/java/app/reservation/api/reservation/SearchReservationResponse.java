package app.reservation.api.reservation;

import core.framework.api.json.Property;
import core.framework.api.validate.NotNull;

import java.time.ZonedDateTime;
import java.util.List;

/**
 * @author joseph
 */
public class SearchReservationResponse {
    @Property(name = "total")
    public Integer total;

    @Property(name = "reservations")
    public List<SearchReservationView> reservations;

    @Property(name = "free_start_times")
    public List<ZonedDateTime> freeStartTimes;

    public static class SearchReservationView {
        @NotNull
        @Property(name = "employee.name")
        public String employeeName;

        @NotNull
        @Property(name = "room.name")
        public String roomName;

        @Property(name = "start_time")
        public ZonedDateTime startTime;

        @Property(name = "end_time")
        public ZonedDateTime endTime;

        @Property(name = "status")
        public ReservationViewStatus status;
    }
}
