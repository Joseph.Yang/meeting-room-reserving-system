package app.reservation.api.reservation;

import core.framework.api.json.Property;
import core.framework.api.validate.NotNull;

import java.time.ZonedDateTime;
import java.util.List;

/**
 * @author joseph
 */
public class BOSearchReservationResponse {
    @Property(name = "total")
    public Integer total;

    @Property(name = "reservations")
    public List<BOSearchReservationView> reservations;

    public static class BOSearchReservationView {
        @NotNull
        @Property(name = "company_name")
        public String companyName;

        @NotNull
        @Property(name = "employee_name")
        public String employeeName;

        @NotNull
        @Property(name = "room_name")
        public String roomName;

        @NotNull
        @Property(name = "start_time")
        public ZonedDateTime startTime;

        @NotNull
        @Property(name = "end_time")
        public ZonedDateTime endTime;

        @NotNull
        @Property(name = "status")
        public ReservationViewStatus status;
    }
}
