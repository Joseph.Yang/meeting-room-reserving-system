package app.reservation.api.reservation;

import core.framework.api.json.Property;
import core.framework.api.validate.NotNull;

import java.time.ZonedDateTime;

/**
 * @author joseph
 */
public class SearchFreeRangeEndTimeRequest {
    @NotNull
    @Property(name = "room_id")
    public Integer roomId;

    @NotNull
    @Property(name = "begin_time")
    public ZonedDateTime beginTime;

    @NotNull
    @Property(name = "company_id")
    public Integer companyId;
}
