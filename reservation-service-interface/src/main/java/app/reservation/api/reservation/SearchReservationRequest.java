package app.reservation.api.reservation;

import core.framework.api.json.Property;
import core.framework.api.validate.NotNull;

import java.time.ZonedDateTime;

/**
 * @author joseph
 */
public class SearchReservationRequest {
    @NotNull
    @Property(name = "room_id")
    public Integer roomId;

    @NotNull
    @Property(name = "company_id")
    public Integer companyId;

    @Property(name = "start_time")
    public ZonedDateTime startTime;

    @Property(name = "end_time")
    public ZonedDateTime endTime;
}
