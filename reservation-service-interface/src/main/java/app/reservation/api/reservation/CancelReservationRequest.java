package app.reservation.api.reservation;

import core.framework.api.json.Property;

/**
 * @author joseph
 */
public class CancelReservationRequest {
    @Property(name = "employee_id")
    public Integer employeeId;
}
