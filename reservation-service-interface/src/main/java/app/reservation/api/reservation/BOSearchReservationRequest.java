package app.reservation.api.reservation;

import core.framework.api.json.Property;
import core.framework.api.validate.Min;
import core.framework.api.validate.NotBlank;
import core.framework.api.validate.NotNull;

/**
 * @author joseph
 */
public class BOSearchReservationRequest {
    @NotNull
    @Min(0)
    @Property(name = "skip")
    public Integer skip = 0;

    @NotNull
    @Min(5)
    @Property(name = "limit")
    public Integer limit = 5;

    @NotNull
    @Property(name = "company_id")
    public Integer companyId;

    @NotNull
    @Property(name = "room_id")
    public Integer roomId;

    @Property(name = "status")
    public ReservationViewStatus status;

    @NotNull
    @NotBlank
    @Property(name = "admin_id")
    public String adminId;
}
