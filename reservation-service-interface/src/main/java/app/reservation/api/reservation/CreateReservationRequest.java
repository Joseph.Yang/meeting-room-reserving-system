package app.reservation.api.reservation;

import core.framework.api.json.Property;
import core.framework.api.validate.NotNull;

import java.time.ZonedDateTime;

/**
 * @author joseph
 */
public class CreateReservationRequest {
    @NotNull
    @Property(name = "employee_id")
    public Integer employeeId;

    @NotNull
    @Property(name = "room_id")
    public Integer roomId;

    @NotNull
    @Property(name = "company_id")
    public Integer companyId;

    @NotNull
    @Property(name = "start_time")
    public ZonedDateTime startTime;

    @NotNull
    @Property(name = "end_time")
    public ZonedDateTime endTime;
}
