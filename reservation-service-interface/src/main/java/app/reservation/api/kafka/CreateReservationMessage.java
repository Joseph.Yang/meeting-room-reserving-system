package app.reservation.api.kafka;

import core.framework.api.json.Property;

import java.time.ZonedDateTime;

public class CreateReservationMessage {
    @Property(name = "employee_email")
    public String employeeEmail;

    @Property(name = "notify_time")
    public ZonedDateTime notifyTime;

    @Property(name = "reservation_id")
    public String reservationId;

    @Property(name = "complete_time")
    public ZonedDateTime completeTime;
}
