package app.reservation.api.kafka;

import core.framework.api.json.Property;

public class CancelReservationMessage {
    @Property(name = "employee_id")
    public Integer employeeId;

    @Property(name = "reservation_id")
    public String reservationId;
}
