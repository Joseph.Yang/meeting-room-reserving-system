package app.reservation.api;

import app.reservation.api.reservation.BOSearchReservationRequest;
import app.reservation.api.reservation.BOSearchReservationResponse;
import app.reservation.api.reservation.BOCancelReservationRequest;
import core.framework.api.web.service.DELETE;
import core.framework.api.web.service.PUT;
import core.framework.api.web.service.Path;
import core.framework.api.web.service.PathParam;

/**
 * @author joseph
 */
public interface BOReservationWebService {
    @PUT
    @Path("/bo/reservation")
    BOSearchReservationResponse search(BOSearchReservationRequest request);

    @DELETE
    @Path("/bo/reservation/:id")
    void delete(@PathParam("id") String id);

    @PUT
    @Path("/bo/reservation/:id")
    void cancel(@PathParam("id") String id, BOCancelReservationRequest request);
}
