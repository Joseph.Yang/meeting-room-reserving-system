package app.reservation.api;

import app.reservation.api.reservation.CancelReservationRequest;
import app.reservation.api.reservation.CreateReservationRequest;
import app.reservation.api.reservation.CreateReservationResponse;
import app.reservation.api.reservation.SearchEmployeeReservationRequest;
import app.reservation.api.reservation.SearchEmployeeReservationsResponse;
import app.reservation.api.reservation.SearchFreeRangeEndTimeRequest;
import app.reservation.api.reservation.SearchFreeRangeEndTimeResponse;
import app.reservation.api.reservation.SearchReservationRequest;
import app.reservation.api.reservation.SearchReservationResponse;
import core.framework.api.http.HTTPStatus;
import core.framework.api.web.service.POST;
import core.framework.api.web.service.PUT;
import core.framework.api.web.service.Path;
import core.framework.api.web.service.PathParam;
import core.framework.api.web.service.ResponseStatus;

/**
 * @author joseph
 */
public interface ReservationWebService {
    @POST
    @Path("/reservation")
    @ResponseStatus(HTTPStatus.CREATED)
    CreateReservationResponse create(CreateReservationRequest request);

    @PUT
    @Path("/reservation")
    SearchReservationResponse search(SearchReservationRequest request);

    @PUT
    @Path("/employee/:id/reservation")
    SearchEmployeeReservationsResponse searchEmployeeReservations(@PathParam("id") Integer id, SearchEmployeeReservationRequest request);

    @PUT
    @Path("/reservation/free-range")
    SearchFreeRangeEndTimeResponse searchFreeRangeEndTime(SearchFreeRangeEndTimeRequest request);

    @PUT
    @Path("/reservation/:id/cancel")
    void cancel(@PathParam("id") String id, CancelReservationRequest request);

    @PUT
    @Path("/reservation/:id/complete")
    void complete(@PathParam("id") String id);
}
