package app.company.api.room;

import core.framework.api.json.Property;

/**
 * @author joseph
 */
public enum RoomViewStatus {
    @Property(name = "OPEN")
    OPEN,
    @Property(name = "CLOSE")
    CLOSE,
    @Property(name = "UNABLE")
    UNABLE
}
