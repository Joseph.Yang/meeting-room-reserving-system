package app.company.api.room;

import core.framework.api.validate.NotBlank;
import core.framework.api.validate.NotNull;
import core.framework.api.web.service.QueryParam;

/**
 * @author joseph
 */
public class BODeleteRoomRequest {
    @NotNull
    @NotBlank
    @QueryParam(name = "admin_id")
    public String adminId;
}
