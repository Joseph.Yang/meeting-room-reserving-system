package app.company.api.room;

import core.framework.api.json.Property;
import core.framework.api.validate.NotNull;

/**
 * @author joseph
 */
public class SearchRoomRequest {
    @NotNull
    @Property(name = "company_id")
    public Integer companyId;
}
