package app.company.api.room;

import core.framework.api.json.Property;
import core.framework.api.validate.NotBlank;
import core.framework.api.validate.NotNull;

import java.util.List;

/**
 * @author joseph
 */
public class BOSearchRoomResponse {
    @Property(name = "total")
    public Integer total;

    @Property(name = "rooms")
    public List<BOSearchRoomView> rooms;

    public static class BOSearchRoomView {
        @NotNull
        @Property(name = "room_id")
        public Integer roomId;

        @NotNull
        @NotBlank
        @Property(name = "room_name")
        public String roomName;

        @NotNull
        @Property(name = "company_id")
        public Integer companyId;

        @NotNull
        @Property(name = "status")
        public RoomViewStatus status;
    }
}
