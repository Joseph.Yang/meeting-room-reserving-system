package app.company.api.room;

import core.framework.api.json.Property;
import core.framework.api.validate.NotNull;

/**
 * @author joseph
 */
public class BOUpdateRoomRequest {
    @NotNull
    @Property(name = "admin_id")
    public String adminId;

    @NotNull
    @Property(name = "status")
    public RoomViewStatus status;
}
