package app.company.api;

import app.company.api.company.BOCreateCompanyRequest;
import app.company.api.company.BOCreateCompanyResponse;
import app.company.api.company.BODeleteCompanyRequest;
import app.company.api.company.BOSearchCompanyRequest;
import app.company.api.company.BOSearchCompanyResponse;
import app.company.api.company.BOUpdateCompanyRequest;
import core.framework.api.http.HTTPStatus;
import core.framework.api.web.service.DELETE;
import core.framework.api.web.service.POST;
import core.framework.api.web.service.PUT;
import core.framework.api.web.service.Path;
import core.framework.api.web.service.PathParam;
import core.framework.api.web.service.ResponseStatus;

/**
 * @author joseph
 */
public interface BOCompanyWebService {
    @POST
    @Path("/bo/company")
    @ResponseStatus(HTTPStatus.CREATED)
    BOCreateCompanyResponse create(BOCreateCompanyRequest request);

    @PUT
    @Path("/bo/company")
    BOSearchCompanyResponse search(BOSearchCompanyRequest request);

    @DELETE
    @Path("/bo/company/:id")
    void delete(@PathParam("id") Integer id, BODeleteCompanyRequest request);

    @PUT
    @Path("/bo/company/:id")
    void update(@PathParam("id") Integer id, BOUpdateCompanyRequest request);
}
