package app.company.api;

import app.company.api.room.RoomView;
import app.company.api.room.SearchRoomResponse;
import core.framework.api.web.service.GET;
import core.framework.api.web.service.Path;
import core.framework.api.web.service.PathParam;

/**
 * @author joseph
 */
public interface RoomWebService {
    @GET
    @Path("/company/:companyId/room/:roomId")
    RoomView get(@PathParam("companyId") Integer companyId, @PathParam("roomId") Integer roomId);

    @GET
    @Path("/company/:companyId/room")
    SearchRoomResponse search(@PathParam("companyId") Integer companyId);
}
