package app.company.api;

import app.company.api.employee.CreateEmployeeRequest;
import app.company.api.employee.CreateEmployeeResponse;
import app.company.api.employee.EmployeeLoginRequest;
import app.company.api.employee.EmployeeLoginResponse;
import app.company.api.employee.EmployeeView;
import app.company.api.employee.ChangePasswordRequest;
import app.company.api.employee.UpdateEmployeeRequest;
import core.framework.api.http.HTTPStatus;
import core.framework.api.web.service.GET;
import core.framework.api.web.service.POST;
import core.framework.api.web.service.PUT;
import core.framework.api.web.service.Path;
import core.framework.api.web.service.PathParam;
import core.framework.api.web.service.ResponseStatus;

/**
 * @author joseph
 */
public interface EmployeeWebService {
    @GET
    @Path("/company/:companyId/employee/:employeeId")
    EmployeeView get(@PathParam("companyId") Integer companyId, @PathParam("employeeId") Integer employeeId);

    @POST
    @Path("/company/:companyId/employee")
    @ResponseStatus(HTTPStatus.CREATED)
    CreateEmployeeResponse create(@PathParam("companyId") Integer companyId, CreateEmployeeRequest request);

    @PUT
    @Path("/company/:companyId/employee/:employeeId")
    void update(@PathParam("companyId") Integer companyId, @PathParam("employeeId") Integer employeeId, UpdateEmployeeRequest request);

    @PUT
    @Path("/company/:companyId/employee/:employeeId/change-password")
    void changePassword(@PathParam("companyId") Integer companyId, @PathParam("employeeId") Integer employeeId, ChangePasswordRequest request);

    @PUT
    @Path("/employee/login")
    EmployeeLoginResponse login(EmployeeLoginRequest request);
}
