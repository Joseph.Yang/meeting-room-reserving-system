package app.company.api;

import app.company.api.room.BOCreateRoomRequest;
import app.company.api.room.BOCreateRoomResponse;
import app.company.api.room.BODeleteRoomRequest;
import app.company.api.room.BOSearchRoomRequest;
import app.company.api.room.BOSearchRoomResponse;
import app.company.api.room.BOUpdateRoomRequest;
import core.framework.api.http.HTTPStatus;
import core.framework.api.web.service.DELETE;
import core.framework.api.web.service.POST;
import core.framework.api.web.service.PUT;
import core.framework.api.web.service.Path;
import core.framework.api.web.service.PathParam;
import core.framework.api.web.service.ResponseStatus;

/**
 * @author joseph
 */
public interface BORoomWebService {
    @POST
    @Path("/bo/room")
    @ResponseStatus(HTTPStatus.CREATED)
    BOCreateRoomResponse create(BOCreateRoomRequest request);

    @PUT
    @Path("/bo/room")
    BOSearchRoomResponse search(BOSearchRoomRequest request);

    @DELETE
    @Path("/bo/room/:id")
    void delete(@PathParam("id") Integer id, BODeleteRoomRequest request);

    @PUT
    @Path("/bo/room/:id")
    void update(@PathParam("id") Integer id, BOUpdateRoomRequest request);
}
