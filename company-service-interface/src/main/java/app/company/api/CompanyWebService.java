package app.company.api;

import app.company.api.company.CompanyView;
import app.company.api.company.SearchCompanyRequest;
import app.company.api.company.SearchCompanyResponse;
import core.framework.api.web.service.GET;
import core.framework.api.web.service.PUT;
import core.framework.api.web.service.Path;
import core.framework.api.web.service.PathParam;

/**
 * @author joseph
 */
public interface CompanyWebService {
    @GET
    @Path("/company/:companyId")
    CompanyView get(@PathParam("companyId") Integer companyId);

    @PUT
    @Path("/company")
    SearchCompanyResponse search(SearchCompanyRequest request);
}
