package app.company.api;

import app.company.api.employee.BODeleteEmployeeRequest;
import app.company.api.employee.BOResetPasswordRequest;
import app.company.api.employee.BOSearchEmployeeRequest;
import app.company.api.employee.BOSearchEmployeeResponse;
import app.company.api.employee.BOUpdateEmployeeRequest;
import core.framework.api.web.service.DELETE;
import core.framework.api.web.service.PUT;
import core.framework.api.web.service.Path;
import core.framework.api.web.service.PathParam;

/**
 * @author joseph
 */
public interface BOEmployeeWebService {
    @PUT
    @Path("/bo/employee/:id")
    void update(@PathParam("id") Integer id, BOUpdateEmployeeRequest request);

    @PUT
    @Path("/bo/employee/:id/reset-password")
    void resetPassword(@PathParam("id") Integer id, BOResetPasswordRequest request);

    @PUT
    @Path("/bo/employee")
    BOSearchEmployeeResponse search(BOSearchEmployeeRequest request);

    @DELETE
    @Path("/bo/employee/:id")
    void delete(@PathParam("id") Integer id, BODeleteEmployeeRequest request);
}
