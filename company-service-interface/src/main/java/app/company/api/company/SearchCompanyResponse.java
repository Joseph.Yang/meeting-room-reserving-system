package app.company.api.company;

import core.framework.api.json.Property;
import core.framework.api.validate.NotBlank;
import core.framework.api.validate.NotNull;

import java.time.ZonedDateTime;
import java.util.List;

/**
 * @author joseph
 */
public class SearchCompanyResponse {
    @Property(name = "total")
    public Integer total;

    @Property(name = "companies")
    public List<SearchCompanyView> companies;

    public static class SearchCompanyView {
        @NotNull
        @Property(name = "company_id")
        public Integer companyId;

        @NotNull
        @NotBlank
        @Property(name = "company_name")
        public String companyName;

        @NotNull
        @NotBlank
        @Property(name = "address")
        public String address;

        @NotNull
        @Property(name = "create_time")
        public ZonedDateTime createTime;
    }
}
