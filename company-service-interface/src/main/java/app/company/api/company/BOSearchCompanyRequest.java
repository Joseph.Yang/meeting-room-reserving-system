package app.company.api.company;

import core.framework.api.json.Property;
import core.framework.api.validate.Min;
import core.framework.api.validate.NotBlank;
import core.framework.api.validate.NotNull;

/**
 * @author joseph
 */
public class BOSearchCompanyRequest {
    @NotNull
    @Min(0)
    @Property(name = "skip")
    public Integer skip;

    @NotNull
    @Min(5)
    @Property(name = "limit")
    public Integer limit;

    @NotNull
    @NotBlank
    @Property(name = "admin_id")
    public String adminId;
}
