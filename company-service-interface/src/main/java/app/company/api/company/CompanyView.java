package app.company.api.company;

import core.framework.api.json.Property;
import core.framework.api.validate.NotBlank;
import core.framework.api.validate.NotNull;

import java.time.ZonedDateTime;

/**
 * @author joseph
 */
public class CompanyView {
    @NotNull
    @Property(name = "company_id")
    public Integer companyId;

    @NotNull
    @NotBlank
    @Property(name = "company_name")
    public String companyName;

    @NotNull
    @NotBlank
    @Property(name = "address")
    public String address;

    @NotNull
    @Property(name = "create_time")
    public ZonedDateTime createTime;

    @NotNull
    @Property(name = "update_time")
    public ZonedDateTime updateTime;

    @NotNull
    @NotBlank
    @Property(name = "admin_id")
    public String adminId;
}
