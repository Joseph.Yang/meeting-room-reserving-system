package app.company.api.company;

import core.framework.api.json.Property;
import core.framework.api.validate.NotBlank;
import core.framework.api.validate.NotNull;

/**
 * @author joseph
 */
public class BOUpdateCompanyRequest {
    @Property(name = "company_name")
    public String companyName;

    @Property(name = "company_address")
    public String companyAddress;

    @NotNull
    @NotBlank
    @Property(name = "admin_id")
    public String adminId;
}
