package app.company.api.company;

import core.framework.api.json.Property;
import core.framework.api.validate.NotBlank;
import core.framework.api.validate.NotNull;

/**
 * @author joseph
 */
public class BOCreateCompanyRequest {
    @NotNull
    @NotBlank
    @Property(name = "company_name")
    public String companyName;

    @NotNull
    @NotBlank
    @Property(name = "company_address")
    public String companyAddress;

    @NotNull
    @Property(name = "admin_id")
    public String adminId;

}
