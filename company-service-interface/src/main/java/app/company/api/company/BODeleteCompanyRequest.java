package app.company.api.company;

import core.framework.api.validate.NotBlank;
import core.framework.api.validate.NotNull;
import core.framework.api.web.service.QueryParam;

/**
 * @author joseph
 */
public class BODeleteCompanyRequest {
    @NotNull
    @NotBlank
    @QueryParam(name = "admin_id")
    public String adminId;
}
