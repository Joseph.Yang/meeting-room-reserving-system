package app.company.api.company;

import core.framework.api.json.Property;
import core.framework.api.validate.Min;
import core.framework.api.validate.NotNull;

/**
 * @author joseph
 */
public class SearchCompanyRequest {
    @NotNull
    @Min(0)
    @Property(name = "skip")
    public Integer skip;

    @NotNull
    @Min(5)
    @Property(name = "limit")
    public Integer limit;

    @Property(name = "company_name")
    public String companyName;
}
