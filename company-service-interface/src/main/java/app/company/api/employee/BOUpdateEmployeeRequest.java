package app.company.api.employee;

import core.framework.api.json.Property;
import core.framework.api.validate.NotBlank;
import core.framework.api.validate.NotNull;

/**
 * @author joseph
 */
public class BOUpdateEmployeeRequest {
    @NotNull
    @Property(name = "status")
    public EmployeeViewStatus status;

    @NotNull
    @NotBlank
    @Property(name = "admin_id")
    public String adminId;
}
