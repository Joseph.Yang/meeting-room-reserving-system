package app.company.api.employee;

import core.framework.api.json.Property;
import core.framework.api.validate.NotBlank;
import core.framework.api.validate.NotNull;

/**
 * @author joseph
 */
public class EmployeeView {
    @NotNull
    @Property(name = "employee_id")
    public Integer employeeId;

    @NotNull
    @NotBlank
    @Property(name = "employee_name")
    public String employeeName;

    @NotNull
    @NotBlank
    @Property(name = "email")
    public String email;

    @NotNull
    @Property(name = "company_id")
    public Integer companyId;
}
