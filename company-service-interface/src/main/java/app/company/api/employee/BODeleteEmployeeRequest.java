package app.company.api.employee;

import core.framework.api.validate.NotBlank;
import core.framework.api.validate.NotNull;
import core.framework.api.web.service.QueryParam;

/**
 * @author joseph
 */
public class BODeleteEmployeeRequest {
    @NotNull
    @NotBlank
    @QueryParam(name = "admin_id")
    public String adminId;
}
