package app.company.api.employee;

import core.framework.api.json.Property;
import core.framework.api.validate.NotBlank;
import core.framework.api.validate.NotNull;

import java.time.ZonedDateTime;
import java.util.List;

/**
 * @author joseph
 */
public class BOSearchEmployeeResponse {
    @Property(name = "total")
    public Integer total;

    @Property(name = "employees")
    public List<BOSearchEmployeeView> employees;

    public static class BOSearchEmployeeView {
        @NotNull
        @Property(name = "employee_id")
        public Integer employeeId;

        @NotNull
        @NotBlank
        @Property(name = "employee_name")
        public String employeeName;

        @NotNull
        @NotBlank
        @Property(name = "email")
        public String email;

        @NotBlank
        @Property(name = "password")
        public String password;

        @NotNull
        @Property(name = "company_id")
        public Integer companyId;

        @NotNull
        @Property(name = "update_time")
        public ZonedDateTime updateTime;

        @NotNull
        @Property(name = "status")
        public EmployeeViewStatus status;
    }
}
