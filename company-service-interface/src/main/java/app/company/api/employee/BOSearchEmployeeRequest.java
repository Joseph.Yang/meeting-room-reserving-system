package app.company.api.employee;

import core.framework.api.json.Property;
import core.framework.api.validate.Min;
import core.framework.api.validate.NotNull;

/**
 * @author joseph
 */
public class BOSearchEmployeeRequest {
    @NotNull
    @Min(0)
    @Property(name = "skip")
    public Integer skip;

    @NotNull
    @Min(10)
    @Property(name = "limit")
    public Integer limit;

    @Property(name = "employee_name")
    public String employeeName;

    @NotNull
    @Property(name = "company_id")
    public Integer companyId;

    @NotNull
    @Property(name = "admin_id")
    public String adminId;
}
