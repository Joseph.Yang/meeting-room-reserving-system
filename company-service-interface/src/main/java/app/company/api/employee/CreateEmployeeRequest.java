package app.company.api.employee;

import core.framework.api.json.Property;
import core.framework.api.validate.NotBlank;
import core.framework.api.validate.NotNull;

/**
 * @author joseph
 */
public class CreateEmployeeRequest {
    @NotNull
    @NotBlank
    @Property(name = "employee_name")
    public String employeeName;

    @NotNull
    @NotBlank
    @Property(name = "password")
    public String password;

    @NotNull
    @NotBlank
    @Property(name = "email")
    public String email;
}
