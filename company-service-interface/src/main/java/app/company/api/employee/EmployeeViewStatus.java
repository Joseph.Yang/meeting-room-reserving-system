package app.company.api.employee;

import core.framework.api.json.Property;

/**
 * @author joseph
 */
public enum EmployeeViewStatus {
    @Property(name = "ACTIVE")
    ACTIVE,

    @Property(name = "INACTIVE")
    INACTIVE
}
