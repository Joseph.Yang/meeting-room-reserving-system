package app.company.api.employee;

import core.framework.api.json.Property;

/**
 * @author joseph
 */
public class UpdateEmployeeRequest {
    @Property(name = "email")
    public String email;

    @Property(name = "employee_name")
    public String employeeName;

    @Property(name = "checked_password")
    public String checkedPassword;
}
