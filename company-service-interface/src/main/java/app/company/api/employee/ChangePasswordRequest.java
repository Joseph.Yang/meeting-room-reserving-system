package app.company.api.employee;

import core.framework.api.json.Property;
import core.framework.api.validate.NotBlank;
import core.framework.api.validate.NotNull;

/**
 * @author joseph
 */
public class ChangePasswordRequest {
    @NotNull
    @NotBlank
    @Property(name = "old_password")
    public String oldPassword;

    @NotNull
    @NotBlank
    @Property(name = "new_password")
    public String newPassword;
}
