package app;

import app.admin.domain.Admin;
import app.admin.service.BOAdminService;
import app.admin.service.admin.BOAdminLoginRequest;
import app.admin.service.admin.BOAdminLoginResponse;
import app.company.api.BOCompanyWebService;
import app.company.api.BOEmployeeWebService;
import app.company.api.BORoomWebService;
import app.company.web.ajax.BOAdminAJAXController;
import app.company.web.ajax.BOCompanyAJAXController;
import app.company.web.ajax.BOEmployeeAJAXController;
import app.company.web.ajax.BOReservationAJAXController;
import app.company.web.ajax.BORoomAJAXController;
import app.company.web.ajax.company.BOCreateCompanyAJAXRequest;
import app.company.web.ajax.company.BOSearchCompanyAJAXRequest;
import app.company.web.ajax.company.BOUpdateCompanyAJAXRequest;
import app.company.web.ajax.employee.BOSearchEmployeeAJAXRequest;
import app.company.web.ajax.employee.BOUpdateEmployeeAJAXRequest;
import app.company.web.ajax.reservation.BOSearchReservationAJAXRequest;
import app.company.web.ajax.room.BOCreateRoomAJAXRequest;
import app.company.web.ajax.room.BOSearchRoomAJAXRequest;
import app.company.web.ajax.room.BOUpdateRoomAJAXRequest;
import app.company.web.interceptor.AdminLoginInterceptor;
import app.reservation.api.BOReservationWebService;
import core.framework.http.HTTPMethod;
import core.framework.module.Module;

/**
 * @author joseph
 */
public class BOCompanyModule extends Module {
    @Override
    protected void initialize() {
        api().client(BOEmployeeWebService.class, requiredProperty("app.companyWebService.url"));
        api().client(BOCompanyWebService.class, requiredProperty("app.companyWebService.url"));
        api().client(BORoomWebService.class, requiredProperty("app.companyWebService.url"));
        api().client(BOReservationWebService.class, requiredProperty("app.reservationWebService.url"));

        db().repository(Admin.class);
        bind(BOAdminService.class);

        http().intercept(bind(AdminLoginInterceptor.class));

        http().bean(BOAdminLoginRequest.class);
        http().bean(BOAdminLoginResponse.class);
        http().bean(BOSearchEmployeeAJAXRequest.class);
        http().bean(BOUpdateEmployeeAJAXRequest.class);
        http().bean(BOCreateCompanyAJAXRequest.class);
        http().bean(BOSearchCompanyAJAXRequest.class);
        http().bean(BOUpdateCompanyAJAXRequest.class);
        http().bean(BOCreateRoomAJAXRequest.class);
        http().bean(BOSearchRoomAJAXRequest.class);
        http().bean(BOUpdateRoomAJAXRequest.class);
        http().bean(BOSearchReservationAJAXRequest.class);

        admin();
        company();
        room();
        employee();
        reservation();
    }

    private void admin() {
        BOAdminAJAXController boAdminAJAXController = bind(BOAdminAJAXController.class);
        http().route(HTTPMethod.PUT, "/admin/login", boAdminAJAXController::login);
        http().route(HTTPMethod.GET, "/admin/logout", boAdminAJAXController::logout);
    }

    private void employee() {
        BOEmployeeAJAXController boEmployeeAJAXController = bind(BOEmployeeAJAXController.class);
        http().route(HTTPMethod.PUT, "/ajax/employee/:id", boEmployeeAJAXController::update);
        http().route(HTTPMethod.PUT, "/ajax/employee/:id/reset-password", boEmployeeAJAXController::resetPassword);
        http().route(HTTPMethod.DELETE, "/ajax/employee/:id", boEmployeeAJAXController::delete);
        http().route(HTTPMethod.PUT, "/ajax/employee", boEmployeeAJAXController::search);
    }

    private void reservation() {
        BOReservationAJAXController boReservationAJAXController = bind(BOReservationAJAXController.class);
        http().route(HTTPMethod.PUT, "/ajax/reservation", boReservationAJAXController::search);
        http().route(HTTPMethod.PUT, "/ajax/reservation/:id", boReservationAJAXController::cancel);
        http().route(HTTPMethod.DELETE, "/ajax/reservation/:id", boReservationAJAXController::delete);
    }

    private void company() {
        BOCompanyAJAXController boCompanyAJAXController = bind(BOCompanyAJAXController.class);
        http().route(HTTPMethod.POST, "/ajax/company", boCompanyAJAXController::create);
        http().route(HTTPMethod.PUT, "/ajax/company/:id", boCompanyAJAXController::update);
        http().route(HTTPMethod.PUT, "/ajax/company", boCompanyAJAXController::search);
        http().route(HTTPMethod.DELETE, "/ajax/company/:id", boCompanyAJAXController::delete);
    }

    private void room() {
        BORoomAJAXController boRoomAJAXController = bind(BORoomAJAXController.class);
        http().route(HTTPMethod.POST, "/ajax/room", boRoomAJAXController::create);
        http().route(HTTPMethod.DELETE, "/ajax/room/:id", boRoomAJAXController::delete);
        http().route(HTTPMethod.PUT, "/ajax/room", boRoomAJAXController::search);
        http().route(HTTPMethod.PUT, "/ajax/room/:id", boRoomAJAXController::update);
    }
}
