package app.company.web.interceptor;

import core.framework.web.Interceptor;
import core.framework.web.Invocation;
import core.framework.web.Response;

public class AdminLoginInterceptor implements Interceptor {
    @Override
    public Response intercept(Invocation invocation) throws Exception {
        String path = invocation.context().request().path();
        if (!"/admin/login".equals(path) && invocation.context().request().session().get("adminId").isEmpty())
            return Response.text("You should login first");
        return invocation.proceed();
    }
}
