package app.company.web.ajax;

import app.company.api.BOCompanyWebService;
import app.company.api.company.BOCreateCompanyRequest;
import app.company.api.company.BODeleteCompanyRequest;
import app.company.api.company.BOSearchCompanyRequest;
import app.company.api.company.BOUpdateCompanyRequest;
import app.company.web.ajax.company.BOCreateCompanyAJAXRequest;
import app.company.web.ajax.company.BOSearchCompanyAJAXRequest;
import app.company.web.ajax.company.BOUpdateCompanyAJAXRequest;
import core.framework.inject.Inject;
import core.framework.web.Request;
import core.framework.web.Response;
import core.framework.web.exception.NotFoundException;

public class BOCompanyAJAXController {
    @Inject
    private BOCompanyWebService companyWebService;

    public Response search(Request request) {
        String adminId = request.session().get("adminId").orElseThrow(() -> new NotFoundException("Session is null"));
        BOSearchCompanyAJAXRequest boSearchCompanyAJAXRequest = request.bean(BOSearchCompanyAJAXRequest.class);
        BOSearchCompanyRequest boSearchCompanyRequest = new BOSearchCompanyRequest();
        boSearchCompanyRequest.limit = boSearchCompanyAJAXRequest.limit;
        boSearchCompanyRequest.skip = boSearchCompanyAJAXRequest.skip;
        boSearchCompanyRequest.adminId = adminId;
        return Response.bean(companyWebService.search(boSearchCompanyRequest));
    }

    public Response delete(Request request) {
        int companyId = Integer.parseInt(request.pathParam("id"));
        String adminId = request.session().get("adminId").orElseThrow();
        BODeleteCompanyRequest boDeleteCompanyRequest = new BODeleteCompanyRequest();
        boDeleteCompanyRequest.adminId = adminId;
        companyWebService.delete(companyId, boDeleteCompanyRequest);
        return Response.text("Success");
    }

    public Response create(Request request) {
        String adminId = request.session().get("adminId").orElseThrow();
        BOCreateCompanyAJAXRequest boCreateCompanyAJAXRequest = request.bean(BOCreateCompanyAJAXRequest.class);
        BOCreateCompanyRequest boCreateCompanyRequest = new BOCreateCompanyRequest();
        boCreateCompanyRequest.companyName = boCreateCompanyAJAXRequest.companyName;
        boCreateCompanyRequest.companyAddress = boCreateCompanyAJAXRequest.companyAddress;
        boCreateCompanyRequest.adminId = adminId;
        return Response.bean(companyWebService.create(boCreateCompanyRequest));
    }

    public Response update(Request request) {
        int companyId = Integer.parseInt(request.pathParam("id"));
        String adminId = request.session().get("adminId").orElseThrow();
        BOUpdateCompanyAJAXRequest boUpdateCompanyAJAXRequest = request.bean(BOUpdateCompanyAJAXRequest.class);
        BOUpdateCompanyRequest boUpdateCompanyRequest = new BOUpdateCompanyRequest();
        boUpdateCompanyRequest.companyAddress = boUpdateCompanyAJAXRequest.companyAddress;
        boUpdateCompanyRequest.companyName = boUpdateCompanyAJAXRequest.companyName;
        boUpdateCompanyRequest.adminId = adminId;
        companyWebService.update(companyId, boUpdateCompanyRequest);
        return Response.text("Success");

    }
}
