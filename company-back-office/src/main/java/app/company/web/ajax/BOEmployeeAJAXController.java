package app.company.web.ajax;

import app.company.api.BOEmployeeWebService;
import app.company.api.employee.BODeleteEmployeeRequest;
import app.company.api.employee.BOResetPasswordRequest;
import app.company.api.employee.BOSearchEmployeeRequest;
import app.company.api.employee.BOUpdateEmployeeRequest;
import app.company.web.ajax.employee.BOSearchEmployeeAJAXRequest;
import app.company.web.ajax.employee.BOUpdateEmployeeAJAXRequest;
import core.framework.inject.Inject;
import core.framework.web.Request;
import core.framework.web.Response;

public class BOEmployeeAJAXController {
    @Inject
    private BOEmployeeWebService employeeWebService;

    public Response search(Request request) {
        String adminId = request.session().get("adminId").orElseThrow();
        BOSearchEmployeeAJAXRequest boSearchEmployeeAJAXRequest = request.bean(BOSearchEmployeeAJAXRequest.class);
        BOSearchEmployeeRequest boSearchEmployeeRequest = new BOSearchEmployeeRequest();
        boSearchEmployeeRequest.limit = boSearchEmployeeAJAXRequest.limit;
        boSearchEmployeeRequest.skip = boSearchEmployeeAJAXRequest.skip;
        boSearchEmployeeRequest.companyId = boSearchEmployeeAJAXRequest.companyId;
        boSearchEmployeeRequest.employeeName = boSearchEmployeeAJAXRequest.employeeName;
        boSearchEmployeeRequest.adminId = adminId;
        return Response.bean(employeeWebService.search(boSearchEmployeeRequest));
    }

    public Response update(Request request) {
        int employeeId = Integer.parseInt(request.pathParam("id"));
        String adminId = request.session().get("adminId").orElseThrow();
        BOUpdateEmployeeAJAXRequest boUpdateEmployeeAJAXRequest = request.bean(BOUpdateEmployeeAJAXRequest.class);
        BOUpdateEmployeeRequest boUpdateEmployeeRequest = new BOUpdateEmployeeRequest();
        boUpdateEmployeeRequest.adminId = adminId;
        boUpdateEmployeeRequest.status = boUpdateEmployeeAJAXRequest.status;
        employeeWebService.update(employeeId, boUpdateEmployeeRequest);
        return Response.text("Success");
    }

    public Response resetPassword(Request request) {
        int employeeId = Integer.parseInt(request.pathParam("id"));
        String adminId = request.session().get("adminId").orElseThrow();
        BOResetPasswordRequest boResetPasswordRequest = new BOResetPasswordRequest();
        boResetPasswordRequest.adminId = adminId;
        employeeWebService.resetPassword(employeeId, boResetPasswordRequest);
        return Response.text("Success");
    }

    public Response delete(Request request) {
        int employeeId = Integer.parseInt(request.pathParam("id"));
        String adminId = request.session().get("adminId").orElseThrow();
        BODeleteEmployeeRequest boDeleteEmployeeRequest = new BODeleteEmployeeRequest();
        boDeleteEmployeeRequest.adminId = adminId;
        employeeWebService.delete(employeeId, boDeleteEmployeeRequest);
        return Response.text("Success");
    }
}