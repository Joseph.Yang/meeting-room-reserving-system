package app.company.web.ajax.room;

import app.company.api.room.RoomViewStatus;
import core.framework.api.json.Property;
import core.framework.api.validate.NotNull;

/**
 * @author joseph
 */
public class BOSearchRoomAJAXRequest {
    @NotNull
    @Property(name = "skip")
    public Integer skip = 0;

    @NotNull
    @Property(name = "limit")
    public Integer limit = 10;

    @Property(name = "status")
    public RoomViewStatus status;

    @Property(name = "company_id")
    public Integer companyId;
}
