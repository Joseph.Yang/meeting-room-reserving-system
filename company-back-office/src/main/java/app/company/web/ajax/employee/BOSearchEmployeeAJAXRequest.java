package app.company.web.ajax.employee;

import core.framework.api.json.Property;
import core.framework.api.validate.NotNull;

/**
 * @author joseph
 */
public class BOSearchEmployeeAJAXRequest {
    @NotNull
    @Property(name = "skip")
    public Integer skip = 0;

    @NotNull
    @Property(name = "limit")
    public Integer limit = 10;

    @Property(name = "employee_name")
    public String employeeName;

    @NotNull
    @Property(name = "company_id")
    public Integer companyId;
}
