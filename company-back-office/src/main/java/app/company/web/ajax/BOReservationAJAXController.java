package app.company.web.ajax;

import app.company.web.ajax.reservation.BOSearchReservationAJAXRequest;
import app.reservation.api.BOReservationWebService;
import app.reservation.api.reservation.BOCancelReservationRequest;
import app.reservation.api.reservation.BOSearchReservationRequest;
import core.framework.inject.Inject;
import core.framework.web.Request;
import core.framework.web.Response;

/**
 * @author joseph
 */
public class BOReservationAJAXController {
    @Inject
    private BOReservationWebService reservationWebService;

    public Response search(Request request) {
        String adminId = request.session().get("adminId").orElseThrow();
        BOSearchReservationAJAXRequest boSearchReservationAJAXRequest = request.bean(BOSearchReservationAJAXRequest.class);
        BOSearchReservationRequest boSearchReservationRequest = new BOSearchReservationRequest();
        boSearchReservationRequest.skip = boSearchReservationAJAXRequest.skip;
        boSearchReservationRequest.limit = boSearchReservationAJAXRequest.limit;
        boSearchReservationRequest.status = boSearchReservationAJAXRequest.status;
        boSearchReservationRequest.companyId = boSearchReservationAJAXRequest.companyId;
        boSearchReservationRequest.roomId = boSearchReservationAJAXRequest.roomId;
        boSearchReservationRequest.adminId = adminId;
        return Response.bean(reservationWebService.search(boSearchReservationRequest));
    }

    public Response delete(Request request) {
        String reservationId = request.pathParam("id");
        reservationWebService.delete(reservationId);
        return Response.text("Success");
    }

    public Response cancel(Request request) {
        String reservationId = request.pathParam("id");
        String adminId = request.session().get("adminId").orElseThrow();
        BOCancelReservationRequest boUpdateReservationRequest = new BOCancelReservationRequest();
        boUpdateReservationRequest.adminId = adminId;
        reservationWebService.cancel(reservationId, boUpdateReservationRequest);
        return Response.text("Success");
    }
}
