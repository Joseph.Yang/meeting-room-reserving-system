package app.company.web.ajax.reservation;

import app.reservation.api.reservation.ReservationViewStatus;
import core.framework.api.json.Property;
import core.framework.api.validate.NotNull;

/**
 * @author joseph
 */
public class BOSearchReservationAJAXRequest {
    @NotNull
    @Property(name = "skip")
    public Integer skip = 0;

    @NotNull
    @Property(name = "limit")
    public Integer limit = 5;

    @Property(name = "company_id")
    public Integer companyId;

    @Property(name = "room_id")
    public Integer roomId;

    @Property(name = "status")
    public ReservationViewStatus status;
}
