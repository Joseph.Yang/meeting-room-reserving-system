package app.company.web.ajax;

import app.admin.service.BOAdminService;
import app.admin.service.admin.BOAdminLoginRequest;
import app.admin.service.admin.BOAdminLoginResponse;
import core.framework.inject.Inject;
import core.framework.web.Request;
import core.framework.web.Response;

/**
 * @author joseph
 */
public class BOAdminAJAXController {
    @Inject
    private BOAdminService boAdminService;

    public Response login(Request request) {
        if (request.session().get("adminId").isPresent()) {
            return Response.text("Admin is already login");
        } else {
            BOAdminLoginRequest boAdminLoginRequest = request.bean(BOAdminLoginRequest.class);
            BOAdminLoginResponse boAdminLoginResponse = boAdminService.login(boAdminLoginRequest);
            request.session().set("adminId", boAdminLoginResponse.id);
            return Response.bean(boAdminLoginResponse);
        }
    }

    public Response logout(Request request) {
        request.session().invalidate();
        return Response.text("Logout");
    }
}

