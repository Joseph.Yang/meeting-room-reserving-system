package app.company.web.ajax.room;

import core.framework.api.json.Property;
import core.framework.api.validate.NotBlank;
import core.framework.api.validate.NotNull;

/**
 * @author joseph
 */
public class BOCreateRoomAJAXRequest {
    @NotNull
    @NotBlank
    @Property(name = "room_name")
    public String roomName;

    @NotNull
    @Property(name = "company_id")
    public Integer companyId;
}
