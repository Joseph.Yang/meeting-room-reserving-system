package app.company.web.ajax.room;

import core.framework.api.json.Property;
import core.framework.api.validate.NotNull;

/**
 * @author joseph
 */
public class BOUpdateRoomAJAXRequest {
    @NotNull
    @Property(name = "status")
    public String status;
}
