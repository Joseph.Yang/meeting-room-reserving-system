package app.company.web.ajax.company;

import core.framework.api.json.Property;

/**
 * @author joseph
 */
public class BOUpdateCompanyAJAXRequest {
    @Property(name = "company_name")
    public String companyName;

    @Property(name = "company_address")
    public String companyAddress;
}
