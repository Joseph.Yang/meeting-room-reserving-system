package app.company.web.ajax.employee;

import app.company.api.employee.EmployeeViewStatus;
import core.framework.api.json.Property;
import core.framework.api.validate.NotNull;

/**
 * @author joseph
 */
public class BOUpdateEmployeeAJAXRequest {
    @NotNull
    @Property(name = "status")
    public EmployeeViewStatus status;
}
