package app.company.web.ajax.company;

import core.framework.api.json.Property;
import core.framework.api.validate.NotNull;

/**
 * @author joseph
 */
public class BOSearchCompanyAJAXRequest {
    @NotNull
    @Property(name = "skip")
    public Integer skip = 0;

    @NotNull
    @Property(name = "limit")
    public Integer limit = 10;
}
