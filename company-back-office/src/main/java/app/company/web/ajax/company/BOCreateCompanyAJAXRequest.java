package app.company.web.ajax.company;

import core.framework.api.json.Property;
import core.framework.api.validate.NotBlank;
import core.framework.api.validate.NotNull;

/**
 * @author joseph
 */
public class BOCreateCompanyAJAXRequest {
    @NotNull
    @NotBlank
    @Property(name = "company_name")
    public String companyName;

    @NotNull
    @NotBlank
    @Property(name = "company_address")
    public String companyAddress;
}
