package app.company.web.ajax;

import app.company.api.BORoomWebService;
import app.company.api.room.BOCreateRoomRequest;
import app.company.api.room.BODeleteRoomRequest;
import app.company.api.room.BOSearchRoomRequest;
import app.company.api.room.BOUpdateRoomRequest;
import app.company.api.room.RoomViewStatus;
import app.company.web.ajax.room.BOCreateRoomAJAXRequest;
import app.company.web.ajax.room.BOSearchRoomAJAXRequest;
import app.company.web.ajax.room.BOUpdateRoomAJAXRequest;
import core.framework.inject.Inject;
import core.framework.web.Request;
import core.framework.web.Response;

/**
 * @author joseph
 */
public class BORoomAJAXController {
    @Inject
    private BORoomWebService roomWebService;

    public Response search(Request request) {
        String adminId = request.session().get("adminId").orElseThrow();
        BOSearchRoomAJAXRequest boSearchRoomAJAXRequest = request.bean(BOSearchRoomAJAXRequest.class);
        BOSearchRoomRequest boSearchRoomRequest = new BOSearchRoomRequest();
        boSearchRoomRequest.limit = boSearchRoomAJAXRequest.limit;
        boSearchRoomRequest.skip = boSearchRoomAJAXRequest.skip;
        boSearchRoomRequest.status = boSearchRoomAJAXRequest.status;
        boSearchRoomRequest.companyId = boSearchRoomAJAXRequest.companyId;
        boSearchRoomRequest.adminId = adminId;
        return Response.bean(roomWebService.search(boSearchRoomRequest));
    }

    public Response delete(Request request) {
        int id = Integer.parseInt(request.pathParam("id"));
        String adminId = request.session().get("adminId").orElseThrow();
        BODeleteRoomRequest boDeleteRoomRequest = new BODeleteRoomRequest();
        boDeleteRoomRequest.adminId = adminId;
        roomWebService.delete(id, boDeleteRoomRequest);
        return Response.text("Success");

    }

    public Response update(Request request) {
        int id = Integer.parseInt(request.pathParam("id"));
        String adminId = request.session().get("adminId").orElseThrow();
        BOUpdateRoomAJAXRequest boUpdateRoomAJAXRequest = request.bean(BOUpdateRoomAJAXRequest.class);
        BOUpdateRoomRequest boUpdateRoomRequest = new BOUpdateRoomRequest();
        boUpdateRoomRequest.status = RoomViewStatus.valueOf(boUpdateRoomAJAXRequest.status);
        boUpdateRoomRequest.adminId = adminId;
        roomWebService.update(id, boUpdateRoomRequest);
        return Response.text("Success");
    }

    public Response create(Request request) {
        String adminId = request.session().get("adminId").orElseThrow();
        BOCreateRoomAJAXRequest boCreateRoomAJAXRequest = request.bean(BOCreateRoomAJAXRequest.class);
        BOCreateRoomRequest createRoomRequest = new BOCreateRoomRequest();
        createRoomRequest.roomName = boCreateRoomAJAXRequest.roomName;
        createRoomRequest.adminId = adminId;
        createRoomRequest.companyId = boCreateRoomAJAXRequest.companyId;
        return Response.bean(roomWebService.create(createRoomRequest));
    }
}
