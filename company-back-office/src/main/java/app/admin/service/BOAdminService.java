package app.admin.service;

import app.admin.domain.Admin;
import app.admin.service.admin.BOAdminLoginRequest;
import app.admin.service.admin.BOAdminLoginResponse;
import app.admin.service.admin.BOCreateAdminRequest;
import core.framework.db.Repository;
import core.framework.inject.Inject;
import core.framework.web.exception.ConflictException;
import core.framework.web.exception.NotFoundException;

import java.util.Optional;
import java.util.UUID;

/**
 * @author joseph
 */
public class BOAdminService {
    @Inject
    private Repository<Admin> adminRepository;

    public BOAdminLoginResponse login(BOAdminLoginRequest request) {
        Optional<Admin> admin = adminRepository.selectOne("name = ?", request.name);
        if (admin.isEmpty())
            throw new NotFoundException("admin not found, name= " + request.name);
        if (admin.get().password.equals(request.password)) {
            BOAdminLoginResponse boAdminLoginResponse = new BOAdminLoginResponse();
            boAdminLoginResponse.id = admin.get().id;
            boAdminLoginResponse.name = admin.get().name;
            return boAdminLoginResponse;
        } else {
            throw new ConflictException("admin name or password is wrong, name= ", request.name);
        }
    }

    public void create(BOCreateAdminRequest request) {
        Optional<Admin> admin = adminRepository.selectOne("name = ?", request.name);
        if (admin.isPresent())
            throw new ConflictException("admin name is already in use, name= " + request.name);
        Admin createAdmin = new Admin();
        createAdmin.id = UUID.randomUUID().toString();
        createAdmin.name = request.name;
        createAdmin.password = request.password;
        adminRepository.insert(createAdmin);
    }
}
