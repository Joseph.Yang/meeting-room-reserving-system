package app.admin.domain;

import core.framework.api.validate.NotNull;
import core.framework.db.Column;
import core.framework.db.PrimaryKey;
import core.framework.db.Table;

/**
 * @author joseph
 */
@Table(name = "admins")
public class Admin {
    @PrimaryKey
    @Column(name = "id")
    public String id;

    @NotNull
    @Column(name = "name")
    public String name;

    @NotNull
    @Column(name = "password")
    public String password;
}
