package app;

import app.company.api.CompanyWebService;
import app.company.api.EmployeeWebService;
import app.company.api.RoomWebService;
import app.company.web.ajax.CompanyAJAXController;
import app.company.web.ajax.EmployeeAJAXController;
import app.company.web.ajax.ReservationAJAXController;
import app.company.web.ajax.RoomAJAXController;
import app.company.web.ajax.company.SearchCompanyAJAXRequest;
import app.company.web.ajax.employee.CreateEmployeeAJAXRequest;
import app.company.web.ajax.employee.EmployeeLoginAJAXRequest;
import app.company.web.ajax.employee.ResetPasswordAJAXRequest;
import app.company.web.ajax.employee.UpdateEmployeeAJAXRequest;
import app.company.web.ajax.reservation.CreateReservationAJAXRequest;
import app.company.web.ajax.reservation.SearchEmployeeReservationAJAXRequest;
import app.company.web.ajax.reservation.SearchFreeRangeEndTimeAJAXRequest;
import app.company.web.ajax.reservation.SearchReservationAJAXRequest;
import app.company.web.interceptor.EmployeeInterceptor;
import app.reservation.api.ReservationWebService;
import core.framework.http.HTTPMethod;
import core.framework.module.Module;

public class CompanyWebModule extends Module {
    @Override
    protected void initialize() {
        api().client(EmployeeWebService.class, requiredProperty("app.companyWebService.url"));
        api().client(CompanyWebService.class, requiredProperty("app.companyWebService.url"));
        api().client(RoomWebService.class, requiredProperty("app.companyWebService.url"));
        api().client(ReservationWebService.class, requiredProperty("app.reservationWebService.url"));

        http().intercept(bind(EmployeeInterceptor.class));

        http().bean(SearchCompanyAJAXRequest.class);
        http().bean(CreateEmployeeAJAXRequest.class);
        http().bean(EmployeeLoginAJAXRequest.class);
        http().bean(UpdateEmployeeAJAXRequest.class);
        http().bean(CreateReservationAJAXRequest.class);
        http().bean(SearchEmployeeReservationAJAXRequest.class);
        http().bean(SearchFreeRangeEndTimeAJAXRequest.class);
        http().bean(SearchReservationAJAXRequest.class);
        http().bean(ResetPasswordAJAXRequest.class);

        company();
        room();
        employee();
        reservation();
    }

    private void employee() {
        EmployeeAJAXController employeeAJAXController = bind(EmployeeAJAXController.class);
        http().route(HTTPMethod.POST, "/ajax/employee/register", employeeAJAXController::register);
        http().route(HTTPMethod.PUT, "/ajax/employee/login", employeeAJAXController::login);
        http().route(HTTPMethod.GET, "/ajax/employee/logout", employeeAJAXController::logout);
        http().route(HTTPMethod.PUT, "/ajax/employee", employeeAJAXController::update);
        http().route(HTTPMethod.PUT, "/ajax/employee/change-password", employeeAJAXController::changePassword);
    }

    private void reservation() {
        ReservationAJAXController reservationAJAXController = bind(ReservationAJAXController.class);
        http().route(HTTPMethod.POST, "/ajax/reservation", reservationAJAXController::create);
        http().route(HTTPMethod.PUT, "/ajax/reservation", reservationAJAXController::search);
        http().route(HTTPMethod.PUT, "/ajax/employee/reservation", reservationAJAXController::searchEmployeeReservations);
        http().route(HTTPMethod.PUT, "/ajax/reservation/:id", reservationAJAXController::cancel);
        http().route(HTTPMethod.PUT, "/ajax/reservation/free-range", reservationAJAXController::searchFreeRangeEndTime);
    }

    private void company() {
        CompanyAJAXController companyAJAXController = bind(CompanyAJAXController.class);
        http().route(HTTPMethod.PUT, "/ajax/company/search", companyAJAXController::search);
        http().route(HTTPMethod.GET, "/ajax/company/:id", companyAJAXController::get);
    }

    private void room() {
        RoomAJAXController roomAJAXController = bind(RoomAJAXController.class);
        http().route(HTTPMethod.GET, "/ajax/room", roomAJAXController::searchRooms);
    }
}
