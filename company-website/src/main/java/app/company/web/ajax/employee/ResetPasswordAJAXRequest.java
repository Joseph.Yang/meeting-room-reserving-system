package app.company.web.ajax.employee;

import core.framework.api.json.Property;
import core.framework.api.validate.NotBlank;
import core.framework.api.validate.NotNull;

/**
 * @author joseph
 */
public class ResetPasswordAJAXRequest {
    @NotNull
    @NotBlank
    @Property(name = "old_password")
    public String oldPassword;

    @NotNull
    @NotBlank
    @Property(name = "new_password")
    public String newPassword;
}
