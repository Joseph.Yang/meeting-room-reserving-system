package app.company.web.ajax;

import app.company.api.employee.EmployeeView;
import app.company.web.ajax.reservation.CreateReservationAJAXRequest;
import app.company.web.ajax.reservation.SearchEmployeeReservationAJAXRequest;
import app.company.web.ajax.reservation.SearchFreeRangeEndTimeAJAXRequest;
import app.company.web.ajax.reservation.SearchReservationAJAXRequest;
import app.reservation.api.ReservationWebService;
import app.reservation.api.reservation.CancelReservationRequest;
import app.reservation.api.reservation.CreateReservationRequest;
import app.reservation.api.reservation.SearchEmployeeReservationRequest;
import app.reservation.api.reservation.SearchFreeRangeEndTimeRequest;
import app.reservation.api.reservation.SearchReservationRequest;
import core.framework.inject.Inject;
import core.framework.json.JSON;
import core.framework.web.Request;
import core.framework.web.Response;

/**
 * @author joseph
 */
public class ReservationAJAXController {
    @Inject
    private ReservationWebService reservationWebService;

    public Response create(Request request) {
        EmployeeView employeeView = JSON.fromJSON(EmployeeView.class, request.session().get("employee").orElseThrow());
        CreateReservationAJAXRequest createReservationAJAXRequest = request.bean(CreateReservationAJAXRequest.class);
        CreateReservationRequest createReservationRequest = new CreateReservationRequest();
        createReservationRequest.startTime = createReservationAJAXRequest.startTime;
        createReservationRequest.endTime = createReservationAJAXRequest.endTime;
        createReservationRequest.roomId = createReservationAJAXRequest.roomId;
        createReservationRequest.employeeId = employeeView.employeeId;
        createReservationRequest.companyId = employeeView.companyId;
        return Response.bean(reservationWebService.create(createReservationRequest));
    }

    public Response cancel(Request request) {
        EmployeeView employeeView = JSON.fromJSON(EmployeeView.class, request.session().get("employee").orElseThrow());
        String reservationId = request.pathParam("id");
        CancelReservationRequest updateReservationRequest = new CancelReservationRequest();
        updateReservationRequest.employeeId = employeeView.employeeId;
        reservationWebService.cancel(reservationId, updateReservationRequest);
        return Response.text("Success");
    }

    public Response search(Request request) {
        EmployeeView employeeView = JSON.fromJSON(EmployeeView.class, request.session().get("employee").orElseThrow());
        SearchReservationAJAXRequest searchReservationAJAXRequest = request.bean(SearchReservationAJAXRequest.class);
        SearchReservationRequest searchReservationRequest = new SearchReservationRequest();
        searchReservationRequest.startTime = searchReservationAJAXRequest.startTime;
        searchReservationRequest.endTime = searchReservationAJAXRequest.endTime;
        searchReservationRequest.roomId = searchReservationAJAXRequest.roomId;
        searchReservationRequest.companyId = employeeView.companyId;
        return Response.bean(reservationWebService.search(searchReservationRequest));
    }

    public Response searchEmployeeReservations(Request request) {
        EmployeeView employeeView = JSON.fromJSON(EmployeeView.class, request.session().get("employee").orElseThrow());
        SearchEmployeeReservationAJAXRequest searchEmployeeReservationAJAXRequest = request.bean(SearchEmployeeReservationAJAXRequest.class);
        SearchEmployeeReservationRequest searchEmployeeReservationRequest = new SearchEmployeeReservationRequest();
        searchEmployeeReservationRequest.status = searchEmployeeReservationAJAXRequest.status;
        searchEmployeeReservationRequest.companyId = employeeView.companyId;
        return Response.bean(reservationWebService.searchEmployeeReservations(employeeView.employeeId, searchEmployeeReservationRequest));
    }

    public Response searchFreeRangeEndTime(Request request) {
        EmployeeView employeeView = JSON.fromJSON(EmployeeView.class, request.session().get("employee").orElseThrow());
        SearchFreeRangeEndTimeAJAXRequest searchFreeRangeEndTimeAJAXRequest = request.bean(SearchFreeRangeEndTimeAJAXRequest.class);
        SearchFreeRangeEndTimeRequest searchFreeRangeEndTimeRequest = new SearchFreeRangeEndTimeRequest();
        searchFreeRangeEndTimeRequest.roomId = searchFreeRangeEndTimeAJAXRequest.roomId;
        searchFreeRangeEndTimeRequest.beginTime = searchFreeRangeEndTimeAJAXRequest.beginTime;
        searchFreeRangeEndTimeRequest.companyId = employeeView.companyId;
        return Response.bean(reservationWebService.searchFreeRangeEndTime(searchFreeRangeEndTimeRequest));
    }
}
