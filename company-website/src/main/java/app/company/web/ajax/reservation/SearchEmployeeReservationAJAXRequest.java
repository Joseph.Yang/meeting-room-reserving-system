package app.company.web.ajax.reservation;

import app.reservation.api.reservation.ReservationViewStatus;
import core.framework.api.json.Property;

/**
 * @author joseph
 */
public class SearchEmployeeReservationAJAXRequest {
    @Property(name = "status")
    public ReservationViewStatus status;
}
