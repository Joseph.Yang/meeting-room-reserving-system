package app.company.web.ajax.reservation;

import core.framework.api.json.Property;
import core.framework.api.validate.NotNull;

import java.time.ZonedDateTime;

/**
 * @author joseph
 */
public class SearchReservationAJAXRequest {
    @NotNull
    @Property(name = "room_id")
    public Integer roomId;

    @Property(name = "start_time")
    public ZonedDateTime startTime;

    @Property(name = "end_time")
    public ZonedDateTime endTime;
}
