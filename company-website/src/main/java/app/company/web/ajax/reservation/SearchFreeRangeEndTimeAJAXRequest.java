package app.company.web.ajax.reservation;

import core.framework.api.json.Property;
import core.framework.api.validate.NotNull;

import java.time.ZonedDateTime;

/**
 * @author joseph
 */
public class SearchFreeRangeEndTimeAJAXRequest {
    @NotNull
    @Property(name = "room_id")
    public Integer roomId;

    @NotNull
    @Property(name = "begin_time")
    public ZonedDateTime beginTime;
}
