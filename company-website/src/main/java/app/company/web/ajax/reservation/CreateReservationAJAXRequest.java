package app.company.web.ajax.reservation;

import core.framework.api.json.Property;
import core.framework.api.validate.NotNull;

import java.time.ZonedDateTime;

/**
 * @author joseph
 */
public class CreateReservationAJAXRequest {
    @NotNull
    @Property(name = "room_id")
    public Integer roomId;

    @NotNull
    @Property(name = "start_time")
    public ZonedDateTime startTime;

    @NotNull
    @Property(name = "end_time")
    public ZonedDateTime endTime;
}
