package app.company.web.ajax;

import app.company.api.CompanyWebService;
import app.company.api.company.SearchCompanyRequest;
import app.company.web.ajax.company.SearchCompanyAJAXRequest;
import core.framework.inject.Inject;
import core.framework.web.Request;
import core.framework.web.Response;

/**
 * @author joseph
 */
public class CompanyAJAXController {
    @Inject
    private CompanyWebService companyWebService;

    public Response get(Request request) {
        int id = Integer.parseInt(request.pathParam("id"));
        return Response.bean(companyWebService.get(id));
    }

    public Response search(Request request) {
        SearchCompanyAJAXRequest searchCompanyAJAXRequest = request.bean(SearchCompanyAJAXRequest.class);
        SearchCompanyRequest searchCompanyRequest = new SearchCompanyRequest();
        searchCompanyRequest.limit = searchCompanyAJAXRequest.limit;
        searchCompanyRequest.skip = searchCompanyAJAXRequest.skip;
        searchCompanyRequest.companyName = searchCompanyAJAXRequest.companyName;
        return Response.bean(companyWebService.search(searchCompanyRequest));
    }
}
