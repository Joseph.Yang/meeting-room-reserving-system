package app.company.web.ajax;

import app.company.api.RoomWebService;
import app.company.api.employee.EmployeeView;
import core.framework.inject.Inject;
import core.framework.json.JSON;
import core.framework.web.Request;
import core.framework.web.Response;

/**
 * @author joseph
 */
public class RoomAJAXController {
    @Inject
    private RoomWebService roomWebService;

    public Response searchRooms(Request request) {
        EmployeeView employeeView = JSON.fromJSON(EmployeeView.class, request.session().get("employee").orElseThrow());
        return Response.bean(roomWebService.search(employeeView.companyId));
    }
}
