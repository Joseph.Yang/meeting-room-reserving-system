package app.company.web.ajax.company;

import core.framework.api.json.Property;
import core.framework.api.validate.Min;
import core.framework.api.validate.NotNull;

/**
 * @author joseph
 */
public class SearchCompanyAJAXRequest {
    @NotNull
    @Min(0)
    @Property(name = "skip")
    public Integer skip = 0;

    @NotNull
    @Min(5)
    @Property(name = "limit")
    public Integer limit = 10;

    @Property(name = "company_name")
    public String companyName;
}
