package app.company.web.ajax;

import app.company.api.EmployeeWebService;
import app.company.api.employee.ChangePasswordRequest;
import app.company.api.employee.CreateEmployeeRequest;
import app.company.api.employee.EmployeeLoginRequest;
import app.company.api.employee.EmployeeLoginResponse;
import app.company.api.employee.EmployeeView;
import app.company.api.employee.UpdateEmployeeRequest;
import app.company.web.ajax.employee.CreateEmployeeAJAXRequest;
import app.company.web.ajax.employee.ResetPasswordAJAXRequest;
import app.company.web.ajax.employee.UpdateEmployeeAJAXRequest;
import core.framework.inject.Inject;
import core.framework.json.JSON;
import core.framework.web.Request;
import core.framework.web.Response;
import core.framework.web.exception.ConflictException;
import core.framework.web.exception.ForbiddenException;
import core.framework.web.service.RemoteServiceException;

/**
 * @author joseph
 */
public class EmployeeAJAXController {
    @Inject
    private EmployeeWebService employeeWebService;

    public Response update(Request request) {
        EmployeeView employeeView = JSON.fromJSON(EmployeeView.class, request.session().get("employee").orElseThrow());
        UpdateEmployeeAJAXRequest updateEmployeeAJAXRequest = request.bean(UpdateEmployeeAJAXRequest.class);
        UpdateEmployeeRequest updateEmployeeRequest = new UpdateEmployeeRequest();
        updateEmployeeRequest.checkedPassword = updateEmployeeAJAXRequest.checkedPassword;
        updateEmployeeRequest.employeeName = updateEmployeeAJAXRequest.employeeName;
        updateEmployeeRequest.email = updateEmployeeAJAXRequest.email;
        try {
            employeeWebService.update(employeeView.companyId, employeeView.employeeId, updateEmployeeRequest);
        } catch (RemoteServiceException e) {
            throw new ForbiddenException(e.getMessage());
        }
        if (updateEmployeeAJAXRequest.email.isBlank())
            employeeView.email = updateEmployeeAJAXRequest.email;
        if (updateEmployeeAJAXRequest.employeeName.isBlank())
            employeeView.employeeName = updateEmployeeAJAXRequest.employeeName;
        request.session().set("employee", JSON.toJSON(employeeView));
        return Response.text("Success");
    }

    public Response changePassword(Request request) {
        EmployeeView employeeView = JSON.fromJSON(EmployeeView.class, request.session().get("employee").orElseThrow());
        ResetPasswordAJAXRequest resetPasswordAJAXRequest = request.bean(ResetPasswordAJAXRequest.class);
        ChangePasswordRequest resetPasswordRequest = new ChangePasswordRequest();
        resetPasswordRequest.oldPassword = resetPasswordAJAXRequest.oldPassword;
        resetPasswordRequest.newPassword = resetPasswordAJAXRequest.newPassword;
        try {
            employeeWebService.changePassword(employeeView.companyId, employeeView.employeeId, resetPasswordRequest);
        } catch (RemoteServiceException e) {
            throw new ForbiddenException(e.getMessage());
        }
        return Response.text("Success");
    }

    public Response register(Request request) {
        CreateEmployeeAJAXRequest createEmployeeAJAXRequest = request.bean(CreateEmployeeAJAXRequest.class);
        CreateEmployeeRequest createEmployeeRequest = new CreateEmployeeRequest();
        createEmployeeRequest.employeeName = createEmployeeAJAXRequest.employeeName;
        createEmployeeRequest.email = createEmployeeAJAXRequest.email;
        createEmployeeRequest.password = createEmployeeAJAXRequest.password;
        try {
            return Response.bean(employeeWebService.create(createEmployeeAJAXRequest.companyId, createEmployeeRequest));
        } catch (RemoteServiceException e) {
            throw new ConflictException(e.getMessage());
        }

    }

    public Response login(Request request) {
        EmployeeLoginRequest employeeLoginRequest = request.bean(EmployeeLoginRequest.class);
        EmployeeLoginResponse employeeLoginResponse = null;
        try {
            employeeLoginResponse = employeeWebService.login(employeeLoginRequest);
        } catch (RemoteServiceException e) {
            throw new ForbiddenException(e.getMessage());
        }
        request.session().set("employee", JSON.toJSON(employeeLoginResponse));
        return Response.bean(employeeLoginResponse);
    }

    public Response logout(Request request) {
        request.session().invalidate();
        return Response.text("Logout");
    }
}

