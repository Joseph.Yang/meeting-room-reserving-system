package app.company.web.interceptor;

import core.framework.web.Interceptor;
import core.framework.web.Invocation;
import core.framework.web.Response;

public class EmployeeInterceptor implements Interceptor {
    @Override
    public Response intercept(Invocation invocation) throws Exception {
        String path = invocation.context().request().path();
        if (!("/ajax/employee/login".equals(path) || "/ajax/employee/register".equals(path) || "/ajax/company/search".equals(path))
            && invocation.context().request().session().get("employee").isEmpty())
            return Response.text("You should login first");
        return invocation.proceed();
    }
}
